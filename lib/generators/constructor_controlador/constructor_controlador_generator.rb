class ConstructorControladorGenerator < Rails::Generators::NamedBase
   source_root File.expand_path('../templates', __FILE__)

  argument :attributes, type: :array, default: [], banner: "field:type field:type"
      
  def variables
    fichero
    puts ARGV
  end
  
  def genera_controlador
    @atributos = Array.new
    @atributos_vista = Array.new
    attributes.each do |attribute| 
      @atributos << ":" + attribute.column_name
      if attribute.name != "activo"
        if attribute.reference?
          @atributos_vista << attribute.name
        else
          @atributos_vista << attribute.column_name   
        end
      end
    end        
    template "constructor_controller.rb", "app/controllers/#{@variables}_controller.rb"  
  end
   
  private
  
  def fichero
    @constructor = file_name.camelize.pluralize
    @variable = file_name
    @variables = file_name.pluralize 
    @modelo = file_name.camelize
  end 
end