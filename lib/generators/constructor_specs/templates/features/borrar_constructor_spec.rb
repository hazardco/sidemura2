require 'spec_helper'

feature 'Borrar <%= @modelo %>' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "<%= @constructor %>", menu: menu, enlace: "/<%= @variables %>")  
    FactoryGirl.create(:<%= @variable %>, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link '<%= @constructor %>'
  end
  
  scenario 'Puedo borrar un <%= @modelo %>' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("<%= @modelo %> borrada correctamente")
  end
end