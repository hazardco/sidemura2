require 'spec_helper'

feature 'Crear <%= @modelo %>' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "<%= @constructor %>", menu: menu, enlace: "/<%= @variables %>")
    visit '/'
    click_link "Genérico"
    click_link '<%= @constructor %>'
    click_link 'Nuevo <%= @modelo %>'
  end
  
  scenario 'Puedo crear un <%= @modelo %>' do
    <% attributes.each do |atributo| %>
      <% if atributo.name != "activo" %>
        fill_in '<%= atributo.name.capitalize %>', with: 'denominación genérica para TEST'
      <% end %>
    <% end %>
    click_button 'Crear <%= @modelo %>'
    
    expect(page).to have_content("<%= @modelo %> añadida correctamente")
  end
  
  scenario "No puedo crear un <%= @modelo %> sin una denominación" do
    <% attributes.each do |atributo| %>
      <% if atributo.name != "activo" %>
        fill_in '<%= atributo.name.capitalize %>', with: ''
      <% end %>
    <% end %>
    click_button 'Crear <%= @modelo %>'
    
    expect(page).to have_content("Hubo un error al añadir el <%= @modelo %>")
    
  end
end