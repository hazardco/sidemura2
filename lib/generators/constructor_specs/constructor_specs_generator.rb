class ConstructorSpecsGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  argument :attributes, type: :array, default: [], banner: "field:type field:type"
      
  def variables
    fichero
    puts ARGV
  end
  
  def genera_test
    template "factories/constructor_factory.rb", "spec/factories/#{@variable}_factory.rb"  
    template "features/borrar_constructor_spec.rb", "spec/features/#{@variable}/borrar_#{@variable}_spec.rb"  
    template "features/crear_constructor_spec.rb", "spec/features/#{@variable}/crear_#{@variable}_spec.rb"  
    template "features/editar_constructor_spec.rb", "spec/features/#{@variable}/editar_#{@variable}_spec.rb"  
    template "features/ver_constructor_spec.rb", "spec/features/#{@variable}/ver_#{@variable}_spec.rb"   
  end
  
  private
  
  def fichero
    @constructor = file_name.camelize.pluralize
    @variable = file_name
    @variables = file_name.pluralize 
    @modelo = file_name.camelize
  end 
end