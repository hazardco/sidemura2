class <%= @constructor %>Datatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: <%= @modelo %>.count,
      iTotalDisplayRecords: <%= @variables %>.total_count,
      aaData: data
    }
  end

private

 def data
    <%= @variables %>.map do |<%= @variable %>|
      [
        <%= @variable %>.id,
        <% attributes.each do |attribute| %>
          <% if attribute.reference? %>
            <%= @variable %>.<%= attribute.name %>.denominacion,
          <% else %>
            <% if attribute.name != "activo" %>
              <%= @variable %>.<%= attribute.name %>,
            <% end %>
          <% end %>
        <% end %>
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/<%= @variables %>/#{<%= @variable %>.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/<%= @variables %>/#{<%= @variable %>.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este <%= @modelo %>?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def <%= @variables %>
    @<%= @variables %> ||= fetch_<%= @variables %>
  end

  def fetch_<%= @variables %>
    <%= @variables %> = <%= @modelo %>.activo.order("#{sort_column} #{sort_direction}")
    <%= @variables %> = <%= @variables %>.page(page).per(per_page)
    if params[:sSearch].present?
      <%= @variables %> = <%= @variables %>.where("id like :sSearch <% attributes.each_with_index do |atributo, index| %><% if !atributo.reference? && atributo.name != "activo" %> or <%= atributo.name %> like :sSearch<% end %><% end %> 
", sSearch: "%#{params[:sSearch]}%" <% attributes.each_with_index do |atributo, index| %><% if !atributo.reference? && atributo.name != "activo" %>, sSearch: "%#{params[:sSearch]}%"<% end %><% end %>)
    end
    if params[:sSearch_0].present? <% attributes.each_with_index do |atributo, index| %><% if !atributo.reference? && atributo.name != "activo" %> || params[:sSearch_<%= index + 1 %>].present? <% end %><% end %>
      <%= @variables %> = <%= @variables %>.where("id like :search_0 <% attributes.each_with_index do |atributo, index| %><% if !atributo.reference? && atributo.name != "activo" %> and <%= atributo.name %> like :search_<%= index + 1 %><% end %><% end %> 
", search_0: "%#{params[:sSearch_0]}%" <% attributes.each_with_index do |atributo, index| %><% if !atributo.reference? && atributo.name != "activo" %>, search_<%= index + 1 %>: "%#{params[:sSearch_<%= index + 1 %>]}%"<% end %><% end %>)
    end
<% attributes.each do |attribute| %>
    <% if attribute.reference? %>
      if params[:sSearch_<%= @atributos_vista.find_index("#{attribute.name}") + 1 %>].present?
      <%= @variables %> = <%= @variables %>.joins(:<%= attribute.name %>).where("<%= attribute.name.pluralize %>.denominacion like :search_<%= @atributos_vista.find_index("#{attribute.name}") + 1 %>", search_<%= @atributos_vista.find_index("#{attribute.name}") + 1 %>: "%#{params[:sSearch_<%= @atributos_vista.find_index("#{attribute.name}") + 1 %>]}%")
    end
    <% end %>
  <% end %>
  
<%= @variables %>
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id <%= @atributos_vista.each{|e| e}.join(" ")%>]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end