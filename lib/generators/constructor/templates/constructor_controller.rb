class <%= @constructor %>Controller < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_<%= @variable %>, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @<%= @variables %> = <%= @modelo %>.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: <%= @constructor %>Datatable.new(view_context) }
    end
  end
  
  def new
    @<%= @variable %> = <%= @modelo %>.new
  end
  
  def edit
  end
  
  def create
    @<%= @variable %> = <%= @modelo %>.new(<%= @variable %>_params)
    if @<%= @variable %>.save
      flash[:notice] = "<%= @modelo %> añadida correctamente"
      redirect_to <%= @variables %>_path
    else
      flash[:alert] = "Hubo un error al añadir el <%= @modelo %>"
      render "new"
    end
  end

  def update
    if @<%= @variable %>.update(<%= @variable %>_params)
      flash[:notice] = "<%= @modelo %> modificada correctamente"
      redirect_to <%= @variables %>_path
    else
      flash[:alert] = "Hubo un error al modificar el <%= @modelo %>"
      render "edit"
    end
  end
  
  def destroy
    @<%= @variable %>.activo = false
    @<%= @variable %>.save
    flash[:notice] = "<%= @modelo %> borrada correctamente"
    redirect_to <%= @variables %>_path
  end
  
  private
  def <%= @variable %>_params
    params.require(:<%= @variable %>).permit(<%= @atributos.each{|e| e}.join(", ") %>)
  end
  
  def set_<%= @variable %>
    @<%= @variable %> = <%= @modelo %>.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa <%= @modelo %>"
    redirect_to <%= @variables %>_path
  end
end
