require 'spec_helper'

feature 'Ver <%= @constructor %>' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "<%= @constructor %>", menu: menu, enlace: "/<%= @variables %>")
  end
      
  scenario 'Puedo ver todos las <%= @constructor %>' do
    FactoryGirl.create(:<%= @variable %>, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:<%= @variable %>, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link '<%= @constructor %>'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end