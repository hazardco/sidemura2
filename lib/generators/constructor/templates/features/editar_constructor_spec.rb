require 'spec_helper'

feature 'Editar <%= @modelo %>' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "<%= @constructor %>", menu: menu, enlace: "/<%= @variables %>")
    FactoryGirl.create(:<%= @variable %>, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link '<%= @constructor %>'
  end
  
  scenario 'Puedo editar un <%= @constructor %>' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    <% attributes.each do |atributo| %>
      <% if atributo.name != "activo" %>
      fill_in '<%= atributo.name.capitalize %>', with: 'nueva denominación genérica para TEST'
      <% end %>
        <% end %>    
        click_button 'Actualizar <%= @modelo %>'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("<%= @modelo %> modificada correctamente")
    end
  
    scenario "No puedo actualizar un <%= @modelo %> sin denominación" do
      click_link "Editar"
      <% attributes.each do |atributo| %>
      <% if atributo.name != "activo" %>
        fill_in '<%= atributo.name.capitalize %>', with: ''
        <% end %>
          <% end %>
          click_button "Actualizar <%= @modelo %>"
        expect(page).to have_content("Hubo un error al modificar el <%= @modelo %>")
      end
    end