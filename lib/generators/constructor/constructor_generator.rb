class ConstructorGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  argument :attributes, type: :array, default: [], banner: "field:type field:type"
      
  def variables
    fichero
    puts ARGV
  end
  
  def genera_modelo
    generate "constructor_modelo", ARGV.join(" ")
  end
  
  def genera_controlador
    generate "constructor_controlador", ARGV.join(" ")  
  end  
  
  def genera_vistas
    generate "constructor_vistas", ARGV.join(" ")
  end
  
  def genera_helpers
    generate "constructor_helpers", ARGV.join(" ")
  end
  
  def genera_javascript
    generate "constructor_javascript", ARGV.join(" ")   
  end
  
  def genera_test
    generate "constructor_specs", ARGV.join(" ")
  end
  
  def genera_rutas
    inject_into_file "config/routes.rb", after: "Sidemura::Application.routes.draw do" do <<-"RUBY"
  

    resources :#{@variables}
      RUBY
    end
  end
  
  def genera_menu
    submenu = Submenu.new
    submenu.denominacion = @modelo
    submenu.icono = "icon-ellipsis-horizontal"
    submenu.enlace = "/#{@variables}"
    submenu.menu_id = 1
    submenu.save
  end
  
  def prepara_base_datos
    rake "db:migrate"
    rake "db:test:prepare"
  end
  
  
  private
  
  def fichero
    @constructor = file_name.camelize.pluralize
    @variable = file_name
    @variables = file_name.pluralize 
    @modelo = file_name.camelize
  end 
end