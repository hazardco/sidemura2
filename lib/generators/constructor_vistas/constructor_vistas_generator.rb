class ConstructorVistasGenerator < Rails::Generators::NamedBase
   source_root File.expand_path('../templates', __FILE__)

  argument :attributes, type: :array, default: [], banner: "field:type field:type"
      
  def variables
    fichero
    puts ARGV
  end
  
  def genera_vistas
    template "views/index.html.erb", "app/views/#{@variables}/index.html.erb"  
    template "views/_tabla.html.erb", "app/views/#{@variables}/_#{@variables}.html.erb"  
    template "views/_boton_atras.html.erb", "app/views/#{@variables}/_boton_atras.html.erb"  
    template "views/_boton_nuevo.html.erb", "app/views/#{@variables}/_boton_nuevo.html.erb"  
    template "views/_errores.html.erb", "app/views/#{@variables}/_errores.html.erb"  
    template "views/_form.html.erb", "app/views/#{@variables}/_form.html.erb"  
    template "views/edit.html.erb", "app/views/#{@variables}/edit.html.erb"  
    template "views/new.html.erb", "app/views/#{@variables}/new.html.erb"  
  end
   
  private
  
  def fichero
    @constructor = file_name.camelize.pluralize
    @variable = file_name
    @variables = file_name.pluralize 
    @modelo = file_name.camelize
  end 
end