class ConstructorJavascriptGenerator < Rails::Generators::NamedBase
   source_root File.expand_path('../templates', __FILE__)

  argument :attributes, type: :array, default: [], banner: "field:type field:type"
      
  def variables
    fichero
    puts ARGV
  end
  
  def atributos
    @atributos = Array.new
    @atributos_vista = Array.new
    attributes.each do |attribute| 
      @atributos << ":" + attribute.column_name
      if attribute.name != "activo"
        if attribute.reference?
          @atributos_vista << attribute.name
        else
          @atributos_vista << attribute.column_name   
        end
      end
    end        
  end
  
  def genera_javascript
    template "javascripts/constructor.js.coffee", "app/assets/javascripts/#{@variables}.js.coffee"  
    template "databases/constructor_datatable.rb", "app/databases/#{@variables}_datatable.rb"     
  end
  
  def anadir_javascript_en_assets

    append_to_file "app/assets/javascripts/datatables.js" do <<-"RUBY"

 //= require #{@variables}.js.coffee
      RUBY
       end
  
  end
   
  private
  
  def fichero
    @constructor = file_name.camelize.pluralize
    @variable = file_name
    @variables = file_name.pluralize 
    @modelo = file_name.camelize
  end 
end