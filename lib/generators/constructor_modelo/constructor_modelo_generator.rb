class ConstructorModeloGenerator < Rails::Generators::NamedBase
   source_root File.expand_path('../templates', __FILE__)

  argument :attributes, type: :array, default: [], banner: "field:type field:type"
      
  def variables
    fichero
    puts ARGV
  end
  
  def genera_modelo
    generate "model", ARGV.join(" ")
    inject_into_file Dir["db/migrate/*_create_#{@variables}.rb"].to_sentence, after: "t.boolean :activo" do <<-'RUBY'
 ,default: true
      RUBY
    end
    
    Dir["db/migrate/*_create_#{@variable}.rb"]
    attributes.each do |atributo|
      if atributo.name != "activo"
        inject_into_file "app/models/#{@variable}.rb", after: "< ActiveRecord::Base" do <<-"RUBY"
      
    validates :#{atributo.name}, presence: true
    
          RUBY
        end
      end
    end
    inject_into_file "app/models/#{@variable}.rb", after: "< ActiveRecord::Base" do <<-'RUBY'
      
  scope :activo, -> { where(activo: true) }
      RUBY
    end
  end
   
  private
  
  def fichero
    @constructor = file_name.camelize.pluralize
    @variable = file_name
    @variables = file_name.pluralize 
    @modelo = file_name.camelize
  end 
end