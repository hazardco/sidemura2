require 'spec_helper'

feature 'Ver Provincias' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Provincias", menu: menu, enlace: "/provincias")
  end
      
  scenario 'Puedo ver todos las Provincias' do
    FactoryGirl.create(:provincia, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:provincia, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Provincias'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end