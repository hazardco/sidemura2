require 'spec_helper'

feature 'Crear Provincia' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Provincias", menu: menu, enlace: "/provincias")
    visit '/'
    click_link "Genérico"
    click_link 'Provincias'
    click_link 'Nuevo Provincia'
  end
  
  scenario 'Puedo crear un Provincia' do
    
      
        fill_in 'Denominacion', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Provincia'
    
    expect(page).to have_content("Provincia añadida correctamente")
  end
  
  scenario "No puedo crear un Provincia sin una denominación" do
    
      
        fill_in 'Denominacion', with: ''
      
    
      
    
    click_button 'Crear Provincia'
    
    expect(page).to have_content("Hubo un error al añadir el Provincia")
    
  end
end