require 'spec_helper'

feature 'Editar Provincia' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Provincias", menu: menu, enlace: "/provincias")
    FactoryGirl.create(:provincia, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Provincias'
  end
  
  scenario 'Puedo editar un Provincias' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Denominacion', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Provincia'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Provincia modificada correctamente")
    end
  
    scenario "No puedo actualizar un Provincia sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Denominacion', with: ''
        
          
      
          
          click_button "Actualizar Provincia"
        expect(page).to have_content("Hubo un error al modificar el Provincia")
      end
    end