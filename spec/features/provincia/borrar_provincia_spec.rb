require 'spec_helper'

feature 'Borrar Provincia' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Provincias", menu: menu, enlace: "/provincias")  
    FactoryGirl.create(:provincia, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Provincias'
  end
  
  scenario 'Puedo borrar un Provincia' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Provincia borrada correctamente")
  end
end