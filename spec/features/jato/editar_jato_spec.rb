require 'spec_helper'

feature 'Editar Jato' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Jatos", menu: menu, enlace: "/jatos")
    FactoryGirl.create(:jato, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Jatos'
  end
  
  scenario 'Puedo editar un Jatos' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Denominacion', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Propio', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Jato'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Jato modificada correctamente")
    end
  
    scenario "No puedo actualizar un Jato sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Denominacion', with: ''
        
          
      
        fill_in 'Propio', with: ''
        
          
      
          
          click_button "Actualizar Jato"
        expect(page).to have_content("Hubo un error al modificar el Jato")
      end
    end