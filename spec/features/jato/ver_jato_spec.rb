require 'spec_helper'

feature 'Ver Jatos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Jatos", menu: menu, enlace: "/jatos")
  end
      
  scenario 'Puedo ver todos las Jatos' do
    FactoryGirl.create(:jato, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:jato, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Jatos'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end