require 'spec_helper'

feature 'Borrar Jato' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Jatos", menu: menu, enlace: "/jatos")  
    FactoryGirl.create(:jato, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Jatos'
  end
  
  scenario 'Puedo borrar un Jato' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Jato borrada correctamente")
  end
end