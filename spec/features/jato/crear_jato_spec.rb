require 'spec_helper'

feature 'Crear Jato' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Jatos", menu: menu, enlace: "/jatos")
    visit '/'
    click_link "Genérico"
    click_link 'Jatos'
    click_link 'Nuevo Jato'
  end
  
  scenario 'Puedo crear un Jato' do
    
      
        fill_in 'Denominacion', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Propio', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Jato'
    
    expect(page).to have_content("Jato añadida correctamente")
  end
  
  scenario "No puedo crear un Jato sin una denominación" do
    
      
        fill_in 'Denominacion', with: ''
      
    
      
        fill_in 'Propio', with: ''
      
    
      
    
    click_button 'Crear Jato'
    
    expect(page).to have_content("Hubo un error al añadir el Jato")
    
  end
end