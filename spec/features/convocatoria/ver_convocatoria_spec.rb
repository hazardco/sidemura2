require 'spec_helper'

feature 'Ver Convocatorias' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Convocatorias", menu: menu, enlace: "/convocatorias")
  end
      
  scenario 'Puedo ver todos las Convocatorias' do
    FactoryGirl.create(:convocatoria, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:convocatoria, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Convocatorias'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end