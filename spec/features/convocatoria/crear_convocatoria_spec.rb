require 'spec_helper'

feature 'Crear Convocatoria' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Convocatorias", menu: menu, enlace: "/convocatorias")
    visit '/'
    click_link "Genérico"
    click_link 'Convocatorias'
    click_link 'Nuevo Convocatoria'
  end
  
  scenario 'Puedo crear un Convocatoria' do
    
      
        fill_in 'Denominacion', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Fecha_doe', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Convocatoria'
    
    expect(page).to have_content("Convocatoria añadida correctamente")
  end
  
  scenario "No puedo crear un Convocatoria sin una denominación" do
    
      
        fill_in 'Denominacion', with: ''
      
    
      
        fill_in 'Fecha_doe', with: ''
      
    
      
    
    click_button 'Crear Convocatoria'
    
    expect(page).to have_content("Hubo un error al añadir el Convocatoria")
    
  end
end