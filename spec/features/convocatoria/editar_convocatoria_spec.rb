require 'spec_helper'

feature 'Editar Convocatoria' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Convocatorias", menu: menu, enlace: "/convocatorias")
    FactoryGirl.create(:convocatoria, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Convocatorias'
  end
  
  scenario 'Puedo editar un Convocatorias' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Denominacion', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Fecha_doe', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Convocatoria'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Convocatoria modificada correctamente")
    end
  
    scenario "No puedo actualizar un Convocatoria sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Denominacion', with: ''
        
          
      
        fill_in 'Fecha_doe', with: ''
        
          
      
          
          click_button "Actualizar Convocatoria"
        expect(page).to have_content("Hubo un error al modificar el Convocatoria")
      end
    end