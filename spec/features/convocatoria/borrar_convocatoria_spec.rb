require 'spec_helper'

feature 'Borrar Convocatoria' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Convocatorias", menu: menu, enlace: "/convocatorias")  
    FactoryGirl.create(:convocatoria, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Convocatorias'
  end
  
  scenario 'Puedo borrar un Convocatoria' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Convocatoria borrada correctamente")
  end
end