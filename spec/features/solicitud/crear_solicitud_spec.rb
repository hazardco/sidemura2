require 'spec_helper'

feature 'Crear Solicitud' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Solicitudes", menu: menu, enlace: "/solicitudes")
    visit '/'
    click_link "Genérico"
    click_link 'Solicitudes'
    click_link 'Nuevo Solicitud'
  end
  
  scenario 'Puedo crear un Solicitud' do
    
      
        fill_in 'Convocatoria', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Turno', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Nif', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Pago', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Estado', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Solicitud'
    
    expect(page).to have_content("Solicitud añadida correctamente")
  end
  
  scenario "No puedo crear un Solicitud sin una denominación" do
    
      
        fill_in 'Convocatoria', with: ''
      
    
      
        fill_in 'Turno', with: ''
      
    
      
        fill_in 'Nif', with: ''
      
    
      
        fill_in 'Pago', with: ''
      
    
      
        fill_in 'Estado', with: ''
      
    
      
    
    click_button 'Crear Solicitud'
    
    expect(page).to have_content("Hubo un error al añadir el Solicitud")
    
  end
end