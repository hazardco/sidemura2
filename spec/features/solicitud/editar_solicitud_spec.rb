require 'spec_helper'

feature 'Editar Solicitud' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Solicitudes", menu: menu, enlace: "/solicitudes")
    FactoryGirl.create(:solicitud, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Solicitudes'
  end
  
  scenario 'Puedo editar un Solicitudes' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Convocatoria', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Turno', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Nif', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Pago', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Estado', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Solicitud'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Solicitud modificada correctamente")
    end
  
    scenario "No puedo actualizar un Solicitud sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Convocatoria', with: ''
        
          
      
        fill_in 'Turno', with: ''
        
          
      
        fill_in 'Nif', with: ''
        
          
      
        fill_in 'Pago', with: ''
        
          
      
        fill_in 'Estado', with: ''
        
          
      
          
          click_button "Actualizar Solicitud"
        expect(page).to have_content("Hubo un error al modificar el Solicitud")
      end
    end