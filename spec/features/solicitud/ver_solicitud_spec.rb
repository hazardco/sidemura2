require 'spec_helper'

feature 'Ver Solicitudes' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Solicitudes", menu: menu, enlace: "/solicitudes")
  end
      
  scenario 'Puedo ver todos las Solicitudes' do
    FactoryGirl.create(:solicitud, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:solicitud, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Solicitudes'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end