require 'spec_helper'

feature 'Borrar Solicitud' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Solicitudes", menu: menu, enlace: "/solicitudes")  
    FactoryGirl.create(:solicitud, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Solicitudes'
  end
  
  scenario 'Puedo borrar un Solicitud' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Solicitud borrada correctamente")
  end
end