require 'spec_helper'

feature 'Borrar Submenu' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    area = FactoryGirl.create(:submenu)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Submenus'
  end
  
  scenario 'Puedo borrar un Submenu' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Submenu borrada correctamente")
  end

end