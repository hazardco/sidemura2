require 'spec_helper'

feature 'Ver Submenus' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Submenus' do
    FactoryGirl.create(:submenu, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:submenu, denominacion: "segundo TEST")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Submenus'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end