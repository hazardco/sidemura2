require 'spec_helper'

feature 'Editar Submenu' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:submenu, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Submenus'
  end
  
  scenario 'Puedo editar un Submenus' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    fill_in "Denominación", with: 'nueva denominación genérica para TEST'
    click_button 'Actualizar Submenu'
    expect(page).to have_content("nueva denominación genérica para TEST")
    expect(page).to have_content("Submenu modificada correctamente")
  end
  
  scenario "No puedo actualizar un Submenu sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Submenu"
    expect(page).to have_content("Hubo un error al modificar el Submenu")
  end
end