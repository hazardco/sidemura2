require 'spec_helper'

feature 'Crear Submenu' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Submenus'
    click_link 'Nueva Submenu'
  end
  
  scenario 'Puedo crear un Submenu' do    
    fill_in 'Denominación', with: 'denominación genérica para TEST'
    click_button 'Crear Submenu'
    
    expect(page).to have_content("Submenu añadida correctamente")
  end
  
  scenario "No puedo crear un Submenu sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Submenu'
    
    expect(page).to have_content("Hubo un error al añadir el Submenu")
    
  end
end