require 'spec_helper'

feature 'Editar Menu' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:menu, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Menus'
  end
  
  scenario 'Puedo editar un Menus' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    fill_in "Denominación", with: 'nueva denominación genérica para TEST'
    click_button 'Actualizar Menu'
    expect(page).to have_content("nueva denominación genérica para TEST")
    expect(page).to have_content("Menu modificada correctamente")
  end
  
  scenario "No puedo actualizar un Menu sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Menu"
    expect(page).to have_content("Hubo un error al modificar el Menu")
  end
end