require 'spec_helper'

feature 'Crear Menu' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Menus'
    click_link 'Nueva Menu'
  end
  
  scenario 'Puedo crear un Menu' do    
    fill_in 'Denominación', with: 'denominación genérica para TEST'
    click_button 'Crear Menu'
    
    expect(page).to have_content("Menu añadida correctamente")
  end
  
  scenario "No puedo crear un Menu sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Menu'
    
    expect(page).to have_content("Hubo un error al añadir el Menu")
    
  end
end