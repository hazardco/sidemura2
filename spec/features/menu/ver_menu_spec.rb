require 'spec_helper'

feature 'Ver Menus' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Menus' do
    FactoryGirl.create(:menu, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:menu, denominacion: "segundo TEST")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Menus'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end