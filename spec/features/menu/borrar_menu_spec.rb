require 'spec_helper'

feature 'Borrar Menu' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    area = FactoryGirl.create(:menu)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Menus'
  end
  
  scenario 'Puedo borrar un Menu' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Menu borrada correctamente")
  end

end