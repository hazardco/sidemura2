require 'spec_helper'

feature 'Ver Magos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Magos", menu: menu, enlace: "/magos")
  end
      
  scenario 'Puedo ver todos las Magos' do
    FactoryGirl.create(:mago, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:mago, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Magos'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end