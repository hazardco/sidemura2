require 'spec_helper'

feature 'Borrar Mago' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Magos", menu: menu, enlace: "/magos")  
    FactoryGirl.create(:mago, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Magos'
  end
  
  scenario 'Puedo borrar un Mago' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Mago borrada correctamente")
  end
end