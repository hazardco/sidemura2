require 'spec_helper'

feature 'Crear Mago' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Magos", menu: menu, enlace: "/magos")
    visit '/'
    click_link "Genérico"
    click_link 'Magos'
    click_link 'Nuevo Mago'
  end
  
  scenario 'Puedo crear un Mago' do
    
      
        fill_in 'Prueba', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Mago'
    
    expect(page).to have_content("Mago añadida correctamente")
  end
  
  scenario "No puedo crear un Mago sin una denominación" do
    
      
        fill_in 'Prueba', with: ''
      
    
      
    
    click_button 'Crear Mago'
    
    expect(page).to have_content("Hubo un error al añadir el Mago")
    
  end
end