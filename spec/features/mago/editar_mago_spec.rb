require 'spec_helper'

feature 'Editar Mago' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Magos", menu: menu, enlace: "/magos")
    FactoryGirl.create(:mago, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Magos'
  end
  
  scenario 'Puedo editar un Magos' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Prueba', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Mago'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Mago modificada correctamente")
    end
  
    scenario "No puedo actualizar un Mago sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Prueba', with: ''
        
          
      
          
          click_button "Actualizar Mago"
        expect(page).to have_content("Hubo un error al modificar el Mago")
      end
    end