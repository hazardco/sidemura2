require 'spec_helper'

feature 'Ver Municipios' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Municipios", menu: menu, enlace: "/municipios")
  end
      
  scenario 'Puedo ver todos las Municipios' do
    FactoryGirl.create(:municipio, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:municipio, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Municipios'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end