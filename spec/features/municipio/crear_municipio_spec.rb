require 'spec_helper'

feature 'Crear Municipio' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Municipios", menu: menu, enlace: "/municipios")
    visit '/'
    click_link "Genérico"
    click_link 'Municipios'
    click_link 'Nuevo Municipio'
  end
  
  scenario 'Puedo crear un Municipio' do
    
      
        fill_in 'Denominacion', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Habitantes', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Provincia', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Municipio'
    
    expect(page).to have_content("Municipio añadida correctamente")
  end
  
  scenario "No puedo crear un Municipio sin una denominación" do
    
      
        fill_in 'Denominacion', with: ''
      
    
      
        fill_in 'Habitantes', with: ''
      
    
      
        fill_in 'Provincia', with: ''
      
    
      
    
    click_button 'Crear Municipio'
    
    expect(page).to have_content("Hubo un error al añadir el Municipio")
    
  end
end