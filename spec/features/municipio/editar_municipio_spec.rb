require 'spec_helper'

feature 'Editar Municipio' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Municipios", menu: menu, enlace: "/municipios")
    FactoryGirl.create(:municipio, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Municipios'
  end
  
  scenario 'Puedo editar un Municipios' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Denominacion', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Habitantes', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Provincia', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Municipio'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Municipio modificada correctamente")
    end
  
    scenario "No puedo actualizar un Municipio sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Denominacion', with: ''
        
          
      
        fill_in 'Habitantes', with: ''
        
          
      
        fill_in 'Provincia', with: ''
        
          
      
          
          click_button "Actualizar Municipio"
        expect(page).to have_content("Hubo un error al modificar el Municipio")
      end
    end