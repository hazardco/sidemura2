require 'spec_helper'

feature 'Borrar Municipio' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Municipios", menu: menu, enlace: "/municipios")  
    FactoryGirl.create(:municipio, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Municipios'
  end
  
  scenario 'Puedo borrar un Municipio' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Municipio borrada correctamente")
  end
end