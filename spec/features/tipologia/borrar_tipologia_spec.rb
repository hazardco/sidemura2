require 'spec_helper'

feature 'Borrar Tipologia' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Tipologias", menu: menu, enlace: "/tipologias")  
    FactoryGirl.create(:tipologia, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Tipologias'
  end
  
  scenario 'Puedo borrar un Tipologia' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Tipologia borrada correctamente")
  end
end