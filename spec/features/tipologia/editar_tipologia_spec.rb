require 'spec_helper'

feature 'Editar Tipologia' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Tipologias", menu: menu, enlace: "/tipologias")
    FactoryGirl.create(:tipologia, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Tipologias'
  end
  
  scenario 'Puedo editar un Tipologias' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Denominacion', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Tipologia'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Tipologia modificada correctamente")
    end
  
    scenario "No puedo actualizar un Tipologia sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Denominacion', with: ''
        
          
      
          
          click_button "Actualizar Tipologia"
        expect(page).to have_content("Hubo un error al modificar el Tipologia")
      end
    end