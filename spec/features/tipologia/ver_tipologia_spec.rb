require 'spec_helper'

feature 'Ver Tipologias' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Tipologias", menu: menu, enlace: "/tipologias")
  end
      
  scenario 'Puedo ver todos las Tipologias' do
    FactoryGirl.create(:tipologia, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:tipologia, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Tipologias'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end