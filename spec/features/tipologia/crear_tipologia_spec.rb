require 'spec_helper'

feature 'Crear Tipologia' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Tipologias", menu: menu, enlace: "/tipologias")
    visit '/'
    click_link "Genérico"
    click_link 'Tipologias'
    click_link 'Nuevo Tipologia'
  end
  
  scenario 'Puedo crear un Tipologia' do
    
      
        fill_in 'Denominacion', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Tipologia'
    
    expect(page).to have_content("Tipologia añadida correctamente")
  end
  
  scenario "No puedo crear un Tipologia sin una denominación" do
    
      
        fill_in 'Denominacion', with: ''
      
    
      
    
    click_button 'Crear Tipologia'
    
    expect(page).to have_content("Hubo un error al añadir el Tipologia")
    
  end
end