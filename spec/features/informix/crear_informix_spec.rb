require 'spec_helper'

feature 'Crear Informix' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Informixes", menu: menu, enlace: "/informixes")
    visit '/'
    click_link "Genérico"
    click_link 'Informixes'
    click_link 'Nuevo Informix'
  end
  
  scenario 'Puedo crear un Informix' do
    
      
        fill_in 'Categoria', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Turno', with: 'denominación genérica para TEST'
      
    
      
        fill_in 'Codigo', with: 'denominación genérica para TEST'
      
    
      
    
    click_button 'Crear Informix'
    
    expect(page).to have_content("Informix añadida correctamente")
  end
  
  scenario "No puedo crear un Informix sin una denominación" do
    
      
        fill_in 'Categoria', with: ''
      
    
      
        fill_in 'Turno', with: ''
      
    
      
        fill_in 'Codigo', with: ''
      
    
      
    
    click_button 'Crear Informix'
    
    expect(page).to have_content("Hubo un error al añadir el Informix")
    
  end
end