require 'spec_helper'

feature 'Editar Informix' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Informixes", menu: menu, enlace: "/informixes")
    FactoryGirl.create(:informix, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Informixes'
  end
  
  scenario 'Puedo editar un Informixes' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    
      
      fill_in 'Categoria', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Turno', with: 'nueva denominación genérica para TEST'
      
        
      
      fill_in 'Codigo', with: 'nueva denominación genérica para TEST'
      
        
      
            
        click_button 'Actualizar Informix'
      expect(page).to have_content("nueva denominación genérica para TEST")
      expect(page).to have_content("Informix modificada correctamente")
    end
  
    scenario "No puedo actualizar un Informix sin denominación" do
      click_link "Editar"
      
      
        fill_in 'Categoria', with: ''
        
          
      
        fill_in 'Turno', with: ''
        
          
      
        fill_in 'Codigo', with: ''
        
          
      
          
          click_button "Actualizar Informix"
        expect(page).to have_content("Hubo un error al modificar el Informix")
      end
    end