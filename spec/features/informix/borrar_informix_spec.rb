require 'spec_helper'

feature 'Borrar Informix' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Informixes", menu: menu, enlace: "/informixes")  
    FactoryGirl.create(:informix, denominacion: "denominación genérica para TEST")

    visit '/'
    click_link "Genérico"
    click_link 'Informixes'
  end
  
  scenario 'Puedo borrar un Informix' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("Informix borrada correctamente")
  end
end