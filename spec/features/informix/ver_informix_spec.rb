require 'spec_helper'

feature 'Ver Informixes' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    menu = FactoryGirl.create(:menu, denominacion: "Genérico")
    FactoryGirl.create(:submenu, denominacion: "Informixes", menu: menu, enlace: "/informixes")
  end
      
  scenario 'Puedo ver todos las Informixes' do
    FactoryGirl.create(:informix, denominacion: "denominación genérica para TEST")
    FactoryGirl.create(:informix, denominacion: "segundo TEST")
    visit '/'
    click_link "Genérico"
    click_link 'Informixes'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end