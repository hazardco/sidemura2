module AutenticacionHelpers
  def autenticar_como!(usuario)
    visit '/login'
    fill_in "login_usuario", with: usuario.usuario
    fill_in "login_password", with: usuario.password
    click_button 'Entrar'
    expect(page).to have_content("Login correcto")
  end 
end
RSpec.configure do |c|
  c.include AutenticacionHelpers, type: :feature
end