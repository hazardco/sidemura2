FactoryGirl.define do
  factory :usuario do
    nombre "Luis Miguel"
    apellidos "Cabezas Granado"
    usuario "luismiguel.cabezas"
    correo "luismiguel.cabezas@gobex.es"
    password "lmcabezas"
    password_confirmation "lmcabezas"
    roles_mask 1
    activo true
  end
end