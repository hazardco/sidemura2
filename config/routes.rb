Sidemura::Application.routes.draw do  

  resources :informixes
  
  resources :motivos
  
  resources :convocatorias
 
  resources :concursos  

  resources :categorias  
  
  resources :titulaciones
  
  get "solicitudes/verificar/:id", to: "solicitudes#verificar_solicitud"
  
  get "solicitudes/verificar", to: "solicitudes#verificar"
 
  get "solicitudes/actualizar_concurso", to: "solicitudes#actualizar_concurso"

  post "solicitudes/verificar", to: "solicitudes#validar"

  post "solicitudes/rechazar", to: "solicitudes#rechazar"
  
  get "solicitudes/ficheros", to: "solicitudes#ficheros"

  post "solicitudes/ficheros", to: "solicitudes#add_ficheros"
  
  post "solicitudes/add_pdf", to: "solicitudes#add_pdf"
  
  get "solicitudes/imprimir/:grupo/:concurso/:turno/:estado", to: "solicitudes#imprimir"

  get 'solicitudes/actualiza_municipios/:id', to: 'solicitudes#actualiza_municipios'

  get 'solicitudes/actualiza_titulaciones/:id', to: 'solicitudes#actualiza_titulaciones'
  
  get 'solicitudes/actualiza_tasas/:id', to: 'solicitudes#actualiza_tasas'

  get 'solicitudes/actualiza_codigo_categoria/:id/:subid', to: 'solicitudes#actualiza_codigo_categoria'
  
  resources :solicitudes
    
  resources :tipologias
  
  resources :municipios 

  resources :provincias
 
  resources :submenus
  
  resources :menus  
  
  resources :usuarios

  get "dashboard/", to: "dashboard#index"

  get "dashboard/real", to: "dashboard#real"

  get "dashboard/charts", to: "dashboard#charts"
  
  get "/login", to: "sesiones#new"
  
  post "/login", to: "sesiones#create"
  
  delete "/login", to: "sesiones#destroy"

  root 'dashboard#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
