# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.irregular 'solicitud', 'solicitudes'
  inflect.irregular 'nacionalidad', 'nacionalidades'
  inflect.irregular 'sexo', 'sexos'
  inflect.irregular 'municipio', 'municipios'
  inflect.irregular 'provincia', 'provincias'
  inflect.irregular 'turno', 'turnos'
  inflect.irregular 'categoria', 'categorias'
  inflect.irregular 'titulacion', 'titulaciones'
  inflect.irregular 'convocatoria', 'convocatorias'
  inflect.irregular 'concurso', 'concursos'  
  inflect.irregular 'permiso', 'permisos'
  inflect.irregular 'zona', 'zonas'
  inflect.irregular 'pago', 'pagos'
  inflect.irregular 'tasa', 'tasas'  
  inflect.irregular 'estado', 'estados'  
  inflect.irregular 'auditoria', 'auditorias'
  inflect.irregular 'informix', 'informixes'
  inflect.irregular 'historico', 'historicos'
end