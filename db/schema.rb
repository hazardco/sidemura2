# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131203083318) do

  create_table "auditorias", force: true do |t|
    t.string   "ip"
    t.string   "sesion"
    t.integer  "estado_id"
    t.boolean  "activo",     default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "auditorias", ["estado_id"], name: "index_auditorias_on_estado_id", using: :btree

  create_table "categorias", force: true do |t|
    t.string   "denominacion"
    t.string   "codigo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "activo",       default: true
    t.boolean  "permiso",      default: false
    t.integer  "tasa_id",      default: 1
  end

  create_table "concursos", force: true do |t|
    t.integer  "categoria_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "modelo_pdf",      default: 1
    t.boolean  "activo",          default: true
    t.integer  "convocatoria_id", default: 1
  end

  add_index "concursos", ["categoria_id"], name: "index_convocatorias_on_categoria_id", using: :btree
  add_index "concursos", ["convocatoria_id"], name: "index_concursos_on_convocatoria_id", using: :btree

  create_table "concursos_titulaciones", id: false, force: true do |t|
    t.integer "concurso_id"
    t.integer "titulacion_id"
  end

  add_index "concursos_titulaciones", ["concurso_id"], name: "index_convocatorias_titulaciones_on_convocatoria_id", using: :btree
  add_index "concursos_titulaciones", ["titulacion_id"], name: "index_convocatorias_titulaciones_on_titulacion_id", using: :btree

  create_table "concursos_turnos", id: false, force: true do |t|
    t.integer "concurso_id"
    t.integer "turno_id"
  end

  create_table "concursos_zonas", id: false, force: true do |t|
    t.integer "concurso_id", null: false
    t.integer "zona_id",     null: false
  end

  create_table "convocatorias", force: true do |t|
    t.string   "denominacion"
    t.date     "fecha_doe"
    t.boolean  "activo",       default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "estados", force: true do |t|
    t.string   "denominacion"
    t.boolean  "activo",       default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "informixes", force: true do |t|
    t.integer  "categoria_id"
    t.integer  "turno_id"
    t.string   "codigo"
    t.boolean  "activo",       default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "informixes", ["categoria_id"], name: "index_informixes_on_categoria_id", using: :btree
  add_index "informixes", ["turno_id"], name: "index_informixes_on_turno_id", using: :btree

  create_table "magos", force: true do |t|
    t.string   "prueba"
    t.boolean  "activo",     default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menus", force: true do |t|
    t.string   "denominacion"
    t.string   "icono"
    t.boolean  "activo",       default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "municipios", force: true do |t|
    t.integer  "provincia_id"
    t.integer  "cmun"
    t.integer  "dc"
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "municipios", ["provincia_id"], name: "index_municipios_on_provincia_id", using: :btree

  create_table "nacionalidades", force: true do |t|
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pagos", force: true do |t|
    t.string   "carta"
    t.string   "verificacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "permisos", force: true do |t|
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "provincias", force: true do |t|
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sexos", force: true do |t|
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "solicitudes", force: true do |t|
    t.string   "nombre"
    t.string   "apellido1"
    t.string   "apellido2"
    t.string   "nif"
    t.date     "fecha_nacimiento"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "telefono"
    t.string   "movil"
    t.string   "domicilio"
    t.string   "numero"
    t.string   "piso"
    t.string   "puerta"
    t.string   "codigo_postal"
    t.integer  "sexo_id"
    t.integer  "nacionalidad_id"
    t.integer  "municipio_id"
    t.integer  "turno_id"
    t.integer  "concurso_id"
    t.integer  "titulacion_id"
    t.integer  "permiso_id"
    t.integer  "grado"
    t.string   "descripcion_discapacidad"
    t.string   "adaptacion"
    t.integer  "pago_id"
    t.integer  "estado_id",                default: 1
    t.boolean  "activo",                   default: true
  end

  add_index "solicitudes", ["concurso_id"], name: "index_solicitudes_on_convocatoria_id", using: :btree
  add_index "solicitudes", ["estado_id"], name: "index_solicitudes_on_estado_id", using: :btree
  add_index "solicitudes", ["municipio_id"], name: "index_solicitudes_on_municipio_id", using: :btree
  add_index "solicitudes", ["nacionalidad_id"], name: "index_solicitudes_on_nacionalidad_id", using: :btree
  add_index "solicitudes", ["pago_id"], name: "index_solicitudes_on_pago_id", using: :btree
  add_index "solicitudes", ["permiso_id"], name: "index_solicitudes_on_permiso_id", using: :btree
  add_index "solicitudes", ["sexo_id"], name: "index_solicitudes_on_sexo_id", using: :btree
  add_index "solicitudes", ["titulacion_id"], name: "index_solicitudes_on_titulacion_id", using: :btree
  add_index "solicitudes", ["turno_id"], name: "index_solicitudes_on_turno_id", using: :btree

  create_table "solicitudes_zonas", id: false, force: true do |t|
    t.integer "solicitud_id"
    t.integer "zona_id"
  end

  add_index "solicitudes_zonas", ["solicitud_id"], name: "index_solicitudes_zonas_on_solicitud_id", using: :btree
  add_index "solicitudes_zonas", ["zona_id"], name: "index_solicitudes_zonas_on_zona_id", using: :btree

  create_table "submenus", force: true do |t|
    t.string   "denominacion"
    t.string   "icono"
    t.string   "enlace"
    t.integer  "menu_id"
    t.boolean  "activo",       default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "submenus", ["menu_id"], name: "index_submenus_on_menu_id", using: :btree

  create_table "tasas", force: true do |t|
    t.string   "denominacion"
    t.string   "importe"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "titulaciones", force: true do |t|
    t.string   "denominacion"
    t.string   "codigo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "activo",       default: true
  end

  create_table "turnos", force: true do |t|
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "nombre"
    t.string   "apellidos"
    t.string   "usuario"
    t.string   "correo"
    t.string   "password_digest"
    t.integer  "roles_mask",      default: 1
    t.boolean  "activo",          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "zonas", force: true do |t|
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "indice_pdf"
  end

end
