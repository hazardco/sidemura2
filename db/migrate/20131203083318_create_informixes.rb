class CreateInformixes < ActiveRecord::Migration
  def change
    create_table :informixes do |t|
      t.references :categoria, index: true
      t.references :turno, index: true
      t.string :codigo
      t.boolean :activo ,default: true


      t.timestamps
    end
  end
end
