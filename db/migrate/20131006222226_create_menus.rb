class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :denominacion
      t.string :icono
      t.boolean :activo ,default: true


      t.timestamps
    end
  end
end
