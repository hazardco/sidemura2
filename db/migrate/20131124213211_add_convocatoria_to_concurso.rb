class AddConvocatoriaToConcurso < ActiveRecord::Migration
  def change
    add_reference :concursos, :convocatoria, index: true
  end
end
