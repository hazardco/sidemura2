class CreateConvocatorias < ActiveRecord::Migration
  def change
    create_table :convocatorias do |t|
      t.string :denominacion
      t.date :fecha_doe
      t.boolean :activo ,default: true


      t.timestamps
    end
  end
end
