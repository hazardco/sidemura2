class CreateSubmenus < ActiveRecord::Migration
  def change
    create_table :submenus do |t|
      t.string :denominacion
      t.string :icono
      t.string :enlace
      t.references :menu, index: true
      t.boolean :activo ,default: true


      t.timestamps
    end
  end
end
