class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nombre
      t.string :apellidos
      t.string :usuario
      t.string :correo
      t.string :password_digest
      t.integer :roles_mask, default: 1
      t.boolean :activo, default: true

      t.timestamps
    end
  end
end
