class SolicitudesDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Solicitud.count,
      iTotalDisplayRecords: solicitudes.total_count,
      aaData: data
    }
  end

private

 def data
    solicitudes.map do |solicitud|
      [
        solicitud.id,
        
          
            solicitud.convocatoria.denominacion,
          
        
          
            solicitud.turno.denominacion,
          
        
          
            
              solicitud.nif,
            
          
        
          
            solicitud.pago.denominacion,
          
        
          
            solicitud.estado.denominacion,
          
        
          
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/solicitudes/#{solicitud.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/solicitudes/#{solicitud.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Solicitud?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def solicitudes
    @solicitudes ||= fetch_solicitudes
  end

  def fetch_solicitudes
    solicitudes = Solicitud.activo.order("#{sort_column} #{sort_direction}")
    solicitudes = solicitudes.page(page).per(per_page)
    if params[:sSearch].present?
      solicitudes = solicitudes.where("id like :sSearch  or nif like :sSearch 
", sSearch: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_3].present? 
      solicitudes = solicitudes.where("id like :search_0  and nif like :search_3 
", search_0: "%#{params[:sSearch_0]}%" , search_3: "%#{params[:sSearch_3]}%")
    end

    
      if params[:sSearch_1].present?
      solicitudes = solicitudes.joins(:convocatoria).where("convocatorias.denominacion like :search_1", search_1: "%#{params[:sSearch_1]}%")
    end
    
  
    
      if params[:sSearch_2].present?
      solicitudes = solicitudes.joins(:turno).where("turnos.denominacion like :search_2", search_2: "%#{params[:sSearch_2]}%")
    end
    
  
    
  
    
      if params[:sSearch_4].present?
      solicitudes = solicitudes.joins(:pago).where("pagos.denominacion like :search_4", search_4: "%#{params[:sSearch_4]}%")
    end
    
  
    
      if params[:sSearch_5].present?
      solicitudes = solicitudes.joins(:estado).where("estados.denominacion like :search_5", search_5: "%#{params[:sSearch_5]}%")
    end
    
  
    
  
  
solicitudes
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id convocatoria turno nif pago estado]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end