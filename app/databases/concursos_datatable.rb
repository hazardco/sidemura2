class ConcursosDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Concurso.count,
      iTotalDisplayRecords: concursos.total_count,
      aaData: data
    }
  end

private

 def data
    concursos.map do |concurso|
      [
        concurso.id,
        
          
            concurso.convocatoria.denominacion,
          
        
          
            concurso.categoria.denominacion,
          
        
          
            concurso.categoria.tasa.denominacion,
          
        
          
            
              concurso.modelo_pdf,
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/concursos/#{concurso.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/concursos/#{concurso.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Concurso?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def concursos
    @concursos ||= fetch_concursos
  end

  def fetch_concursos
    concursos = Concurso.activo.order("#{sort_column} #{sort_direction}")
    concursos = concursos.page(page).per(per_page)
    if params[:sSearch].present?
      concursos = concursos.where("id like :sSearch  or modelo_pdf like :sSearch 
", sSearch: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_4].present? 
      concursos = concursos.where("id like :search_0  and modelo_pdf like :search_4 
", search_0: "%#{params[:sSearch_0]}%" , search_4: "%#{params[:sSearch_4]}%")
    end

    
      if params[:sSearch_1].present?
      concursos = concursos.joins(:convocatoria).where("convocatorias.denominacion like :search_1", search_1: "%#{params[:sSearch_1]}%")
    end
    
  
    
      if params[:sSearch_2].present?
      concursos = concursos.joins(:categoria).where("categorias.denominacion like :search_2", search_2: "%#{params[:sSearch_2]}%")
    end
    
  
    
      if params[:sSearch_3].present?
      concursos = concursos.joins(:tasa).where("tasas.denominacion like :search_3", search_3: "%#{params[:sSearch_3]}%")
    end
    
  
    
  
  
concursos
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id convocatoria categoria tasa modelo_pdf]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end