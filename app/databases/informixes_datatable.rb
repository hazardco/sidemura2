class InformixesDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Informix.count,
      iTotalDisplayRecords: informixes.total_count,
      aaData: data
    }
  end

private

 def data
    informixes.map do |informix|
      [
        informix.id,
        
          
            informix.concurso.categoria.denominacion,
          
        
          
            informix.turno.denominacion,
          
        
          
            
              informix.codigo,
            
          
        
          
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/informixes/#{informix.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/informixes/#{informix.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Informix?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def informixes
    @informixes ||= fetch_informixes
  end

  def fetch_informixes
    informixes = Informix.activo.order("#{sort_column} #{sort_direction}")
    informixes = informixes.page(page).per(per_page)
    if params[:sSearch].present?
      informixes = informixes.where("id like :sSearch  or codigo like :sSearch 
", sSearch: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_3].present? 
      informixes = informixes.where("id like :search_0  and codigo like :search_3 
", search_0: "%#{params[:sSearch_0]}%" , search_3: "%#{params[:sSearch_3]}%")
    end

    
      if params[:sSearch_1].present?
      informixes = informixes.joins(:categoria).where("categorias.denominacion like :search_1", search_1: "%#{params[:sSearch_1]}%")
    end
    
  
    
      if params[:sSearch_2].present?
      informixes = informixes.joins(:turno).where("turnos.denominacion like :search_2", search_2: "%#{params[:sSearch_2]}%")
    end
    
  
    
  
    
  
  
informixes
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id categoria turno codigo]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end