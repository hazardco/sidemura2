class ConvocatoriasDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Convocatoria.count,
      iTotalDisplayRecords: convocatorias.total_count,
      aaData: data
    }
  end

private

 def data
    convocatorias.map do |convocatoria|
      [
        convocatoria.id,
        
          
            
              convocatoria.denominacion,
            
          
        
          
            
              convocatoria.fecha_doe,
            
          
        
          
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/convocatorias/#{convocatoria.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/convocatorias/#{convocatoria.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Convocatoria?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def convocatorias
    @convocatorias ||= fetch_convocatorias
  end

  def fetch_convocatorias
    convocatorias = Convocatoria.activo.order("#{sort_column} #{sort_direction}")
    convocatorias = convocatorias.page(page).per(per_page)
    if params[:sSearch].present?
      convocatorias = convocatorias.where("id like :sSearch  or denominacion like :sSearch or fecha_doe like :sSearch 
", sSearch: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%", sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_1].present?  || params[:sSearch_2].present? 
      convocatorias = convocatorias.where("id like :search_0  and denominacion like :search_1 and fecha_doe like :search_2 
", search_0: "%#{params[:sSearch_0]}%" , search_1: "%#{params[:sSearch_1]}%", search_2: "%#{params[:sSearch_2]}%")
    end

    
  
    
  
    
  
  
convocatorias
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id denominacion fecha_doe]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end