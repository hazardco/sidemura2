class MunicipiosDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Municipio.count,
      iTotalDisplayRecords: municipios.total_count,
      aaData: data
    }
  end

private

 def data
    municipios.map do |municipio|
      [
        municipio.id,
        
          
            
              municipio.denominacion,
            
          
        
          
            
              municipio.habitantes,
            
          
        
          
            municipio.provincia.denominacion,
          
        
          
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/municipios/#{municipio.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/municipios/#{municipio.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Municipio?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def municipios
    @municipios ||= fetch_municipios
  end

  def fetch_municipios
    municipios = Municipio.activo.order("#{sort_column} #{sort_direction}")
    municipios = municipios.page(page).per(per_page)
    if params[:sSearch].present?
      municipios = municipios.where("id like :sSearch  or denominacion like :sSearch or habitantes like :sSearch 
", search_0: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%", sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_1].present?  || params[:sSearch_2].present? 
      municipios = municipios.where("id like :search_0  and denominacion like :search_1 and habitantes like :search_2 
", search_0: "%#{params[:sSearch_0]}%" , search_1: "%#{params[:sSearch_1]}%", search_2: "%#{params[:sSearch_2]}%")
    end

    
  
    
  
    
      if params[:sSearch_3].present?
      municipios = municipios.joins(:provincia).where("provincias.denominacion like :search_3", search_3: "%#{params[:sSearch_3]}%")
    end
    
  
    
  
  
municipios
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id denominacion habitantes provincia]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end