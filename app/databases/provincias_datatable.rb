class ProvinciasDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Provincia.count,
      iTotalDisplayRecords: provincias.total_count,
      aaData: data
    }
  end

private

 def data
    provincias.map do |provincia|
      [
        provincia.id,
        
          
            
              provincia.denominacion,
            
          
        
          
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/provincias/#{provincia.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/provincias/#{provincia.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Provincia?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def provincias
    @provincias ||= fetch_provincias
  end

  def fetch_provincias
    provincias = Provincia.activo.order("#{sort_column} #{sort_direction}")
    provincias = provincias.page(page).per(per_page)
    if params[:sSearch].present?
      provincias = provincias.where("id like :sSearch  or denominacion like :sSearch 
", search_0: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_1].present? 
      provincias = provincias.where("id like :search_0  and denominacion like :search_1 
", search_0: "%#{params[:sSearch_0]}%" , search_1: "%#{params[:sSearch_1]}%")
    end

    
  
    
  
  
provincias
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id denominacion]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end