class MotivosDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Motivo.count,
      iTotalDisplayRecords: motivos.total_count,
      aaData: data
    }
  end

private

 def data
    motivos.map do |motivo|
      [
        motivo.id,
        
          
            
              motivo.denominacion,
            
          
        
          
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/motivos/#{motivo.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/motivos/#{motivo.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Motivo?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def motivos
    @motivos ||= fetch_motivos
  end

  def fetch_motivos
    motivos = Motivo.activo.order("#{sort_column} #{sort_direction}")
    motivos = motivos.page(page).per(per_page)
    if params[:sSearch].present?
      motivos = motivos.where("id like :sSearch  or denominacion like :sSearch 
", sSearch: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_1].present? 
      motivos = motivos.where("id like :search_0  and denominacion like :search_1 
", search_0: "%#{params[:sSearch_0]}%" , search_1: "%#{params[:sSearch_1]}%")
    end

    
  
    
  
  
motivos
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id denominacion]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end