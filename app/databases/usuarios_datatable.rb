class UsuariosDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Usuario.count,
      iTotalDisplayRecords: usuarios.total_count,
      aaData: data
    }
  end

  private

  def data
    usuarios.map do |usuario|
      [
        usuario.id,
        usuario.nombre,
        usuario.apellidos,
        usuario.usuario,
        usuario.roles,
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/usuarios/#{usuario.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/usuarios/#{usuario.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Usuario?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def usuarios
    @usuarios ||= fetch_usuarios
  end

  def fetch_usuarios
    usuarios = Usuario.activo.order("#{sort_column} #{sort_direction}")
    usuarios = usuarios.page(page).per(per_page)
    if params[:sSearch].present?
      usuarios = usuarios.where("id like :sSearch  or nombre like :sSearch or apellidos like :sSearch or usuario like :sSearch
        ", sSearch: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%", sSearch: "%#{params[:sSearch]}%", sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_1].present?  || params[:sSearch_2].present? || params[:sSearch_3].present? 
      usuarios = usuarios.where("id like :search_0  and nombre like :search_1 and apellidos like :search_2 
        ", search_0: "%#{params[:sSearch_0]}%" , search_1: "%#{params[:sSearch_1]}%", search_2: "%#{params[:sSearch_2]}%", search_3: "%#{params[:sSearch_3]}%")
    end
    usuarios
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id nombre apellidos]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end