class CategoriasDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Categoria.count,
      iTotalDisplayRecords: categorias.total_count,
      aaData: data
    }
  end

private

 def data
    categorias.map do |categoria|
      [
        categoria.id,
        
          
            
              categoria.denominacion,
            
          
        
          
            
              categoria.codigo,
            
          
        
          
            
          
        
        "<div class=\"text-right\">                     
         <a id=\"Editar\" href=\"/categorias/#{categoria.id}/edit\" class=\"btn btn-success btn-xs\"> 
           <i class=\"icon-edit\"></i>
         </a> 
         <a rel=\"nofollow\" id=\"Borrar\" href=\"/categorias/#{categoria.id}\" data-method=\"delete\" data-confirm=\"¿Seguro que quieres borrar este Categoria?\" class=\"btn btn-danger btn-xs\">
           <i class=\"icon-remove\"></i>
         </a>
         </div>",
      ]
    end
  end

  def categorias
    @categorias ||= fetch_categorias
  end

  def fetch_categorias
    categorias = Categoria.activo.order("#{sort_column} #{sort_direction}")
    categorias = categorias.page(page).per(per_page)
    if params[:sSearch].present?
      categorias = categorias.where("id like :sSearch  or denominacion like :sSearch or codigo like :sSearch 
", sSearch: "%#{params[:sSearch]}%" , sSearch: "%#{params[:sSearch]}%", sSearch: "%#{params[:sSearch]}%")
    end
    if params[:sSearch_0].present?  || params[:sSearch_1].present?  || params[:sSearch_2].present? 
      categorias = categorias.where("id like :search_0  and denominacion like :search_1 and codigo like :search_2 
", search_0: "%#{params[:sSearch_0]}%" , search_1: "%#{params[:sSearch_1]}%", search_2: "%#{params[:sSearch_2]}%")
    end

    
  
    
  
    
  
  
categorias
  
  


  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id denominacion codigo]
    Rails.logger.info(columns)
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end