module SolicitudesHelper
  def boton_solicitud
    Submenu.where(enlace: "/solicitudes").last.icono
  end
  def codigo_categoria
    categoria = Concurso.activo.joins("inner join categorias on concursos.categoria_id = categorias.id").where("categorias.tasa_id = ?",@solicitud.concurso.categoria.tasa_id).first
    turno = categoria.turnos.first
    Informix.where("concurso_id = ? and turno_id = ?", categoria, turno).first.codigo
  end
end