class ProvinciasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_provincia, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @provincias = Provincia.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: ProvinciasDatatable.new(view_context) }
    end
  end
  
  def new
    @provincia = Provincia.new
  end
  
  def edit
  end
  
  def create
    @provincia = Provincia.new(provincia_params)
    if @provincia.save
      flash[:notice] = "Provincia añadida correctamente"
      redirect_to provincias_path
    else
      flash[:alert] = "Hubo un error al añadir el Provincia"
      render "new"
    end
  end

  def update
    if @provincia.update(provincia_params)
      flash[:notice] = "Provincia modificada correctamente"
      redirect_to provincias_path
    else
      flash[:alert] = "Hubo un error al modificar el Provincia"
      render "edit"
    end
  end
  
  def destroy
    @provincia.activo = false
    @provincia.save
    flash[:notice] = "Provincia borrada correctamente"
    redirect_to provincias_path
  end
  
  private
  def provincia_params
    params.require(:provincia).permit(:denominacion, :activo)
  end
  
  def set_provincia
    @provincia = Provincia.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Provincia"
    redirect_to provincias_path
  end
end
