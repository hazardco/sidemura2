class DashboardController < ApplicationController
  before_action :requiere_autenticacion!
  def index
        @totales_solicitudes = Solicitud.where("estado_id = 3").group("date(created_at)").count

    respond_to do |format|
      format.html
      format.json { render json: DashboardDatatable.new(view_context) }
    end
  end
  
  def real
    
    @solicitudes_nuevas = Auditoria.where("created_at between ? and ? and estado_id = 1", Time.now - 5.minute,Time.now).count
    @solicitudes_verificadas = Auditoria.where("created_at between ? and ? and estado_id = 2", Time.now - 5.minute,Time.now).count
    @solicitudes_pdf = Auditoria.where("created_at between ? and ? and estado_id = 3", Time.now - 5.minute,Time.now).count
    @media_proceso = Auditoria.where("created_at between ? and ? and estado_id = 3", Time.now - 5.minute,Time.now).average("TIME_TO_SEC(TIMEDIFF(updated_at, created_at))").to_i
    respond_to do |format|
      format.js
    end
  end
  
  def charts
    @numero_total_solicitudes = Auditoria.where("browser is NOT NULL and estado_id = 3").count
    @totales = Auditoria.select("browser").where("browser is NOT NULL and estado_id = 3").group("browser").count("browser")
    @totales_solicitudes = Solicitud.where("estado_id = 3").group("date(created_at)").count
    respond_to do |format|
      format.js
    end
  end

end
