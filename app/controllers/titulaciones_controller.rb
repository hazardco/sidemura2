class TitulacionesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_titulacion, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @titulaciones = Titulacion.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: TitulacionesDatatable.new(view_context) }
    end
  end
  
  def new
    @titulacion = Titulacion.new
  end
  
  def edit
  end
  
  def create
    @titulacion = Titulacion.new(titulacion_params)
    if @titulacion.save
      flash[:notice] = "Titulacion añadida correctamente"
      redirect_to titulaciones_path
    else
      flash[:alert] = "Hubo un error al añadir el Titulacion"
      render "new"
    end
  end

  def update
    if @titulacion.update(titulacion_params)
      flash[:notice] = "Titulacion modificada correctamente"
      redirect_to titulaciones_path
    else
      flash[:alert] = "Hubo un error al modificar el Titulacion"
      render "edit"
    end
  end
  
  def destroy
    @titulacion.activo = false
    @titulacion.save
    flash[:notice] = "Titulacion borrada correctamente"
    redirect_to titulaciones_path
  end
  
  private
  def titulacion_params
    params.require(:titulacion).permit(:denominacion, :codigo, :activo)
  end
  
  def set_titulacion
    @titulacion = Titulacion.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Titulacion"
    redirect_to titulaciones_path
  end
end
