class SolicitudesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_solicitud, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def imprimir

    require 'csv'

    where = Array.new
    @grupo = "Todos los Grupos"
    @concurso = "Todas los concursos"
    @turno = "Todos los turnos"
    @estado = "Todos los estados"
    
    if params[:grupo] && params[:grupo] != "0"
      where << "tasa_id = #{params[:grupo]}"
      @grupo = Tasa.find(params[:grupo]).denominacion
    end
    if params[:concurso] && params[:concurso] != "0"
      where << "concurso_id = #{params[:concurso]}"
      @concurso = Concurso.find(params[:concurso]).categoria.denominacion
    end
    if params[:turno] && params[:turno] != "0"
      where << "turno_id = #{params[:turno]}"
      @turno = Turno.find(params[:turno]).denominacion
    end
    if params[:estado] && params[:estado] != "0"
      where << "estado_id = #{params[:estado]}"
      @estado = Estado.find(params[:estado]).denominacion
    end

    @solicitudes = Solicitud.activo.joins("inner join concursos on solicitudes.concurso_id = concursos.id").joins(" inner join categorias on concursos.categoria_id = categorias.id").where(where.join(" and ")).order(:apellido1, :apellido2, :nombre, :nif)
    respond_to do |format|
      format.pdf
      format.xls
    end
  end
  
  def index
    where = Array.new
    if params[:grupo] && params[:grupo] != "0"
      where << "tasa_id = #{params[:grupo]}"
    end
    if params[:concurso] && params[:concurso] != "0"
      where << "concurso_id = #{params[:concurso]}"
    end
    if params[:turno] && params[:turno] != "0"
      where << "turno_id = #{params[:turno]}"
    end
    if params[:estado] && params[:estado] != "0"
      where << "estado_id = #{params[:estado]}"
    end
    if params[:nif_carta_pago] && params[:nif_carta_pago] != "0"
      where << "(nif like '%#{params[:nif_carta_pago]}%' or carta like '%#{params[:nif_carta_pago][0..params[:nif_carta_pago].length-2]}%')"
    end
    @solicitudes = Solicitud.activo.joins("inner join concursos on solicitudes.concurso_id = concursos.id").joins(" inner join categorias on concursos.categoria_id = categorias.id")
    .joins("inner join pagos on solicitudes.pago_id = pagos.id").where(where.join(" and ")).page params[:page]
    respond_to do |format|
      format.js
      format.html
    end
  end
  
  def new
    @solicitud = Solicitud.new
  end
  
  def show
    @historicos = Historico.where(solicitud_id: @solicitud.id).order("created_at DESC")
  end
  
  def edit
    copiar_historico
  end
  
  def update
    @solicitud.update(solicitud_params)
    if !params[:solicitud][:permiso_id]
      @solicitud.permiso_id = 0
    end
    @solicitud.nacionalidad_id= 64 if !params[:solicitud][:nacionalidad_id]
    if params[:fecha_nacimiento]
      @solicitud.fecha_nacimiento = params[:fecha_nacimiento]
    else
      @solicitud.fecha_nacimiento = params[:fecha_nacimiento16]
    end
    if @solicitud.save
      @solicitud.zonas.destroy_all
      if params[:RENUNCIA]
        @zonas = @solicitud.concurso.zonas.where('denominacion = "RENUNCIA"')
      else    
        if params[:zona]
          @zonas = params[:zona]
        else
          @zonas = @solicitud.concurso.zonas.where('denominacion != "RENUNCIA"')
        end  
      end
      @zonas.each do |z|
        zona = Zona.find(z)
        @solicitud.zonas << zona
      end
      redirect_to @solicitud
    else
      flash[:alert] ="Hay algunos errores que impidieron crear la Solicitud"
      render 'edit'
    end
  end
 
  def verificar
    
  end
  
  def ficheros
    
  end
  
  def add_ficheros
    @carta_pago = params[:ficheros][:carta_pago]
    @pago = Pago.where(carta: @carta_pago[0..11])
    if @pago.count == 0
      flash[:alert] = "No existe la carta de pago proporcionada"
      redirect_to solicitudes_ficheros_path
    else
      @solicitud = @pago.first.solicitud
    end
  end
  
  def add_pdf  
    require 'fileutils'
    tmp = params[:ficheros][:fichero].tempfile
    file = File.join("public", params[:ficheros][:fichero].original_filename)
    solicitud = params[:id]
    directorio = solicitud.to_i / 1000
    dir = "public/ficheros/#{directorio.to_s}"
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
    FileUtils.cp tmp.path, "public/ficheros/#{directorio.to_s}/#{solicitud}.pdf"
    flash[:notice] = "Solicitud nº #{solicitud} archivada en pdf"
    redirect_to solicitudes_ficheros_path
  end
  
  def actualizar_concurso
    
  end
  
  def rechazar
    @solicitud = Solicitud.find(params[:id])
    @solicitud.estado_id = 5
    if params[:motivo]
      @motivos = params[:motivo]
         
      @motivos.each do |m|
        motivo = Motivo.find(m)
        @solicitud.motivos << motivo
      end
    end
    @solicitud.save
    ## Grabar en el histórico
    if Historico.where(solicitud_id: @solicitud.id).count == 0
      historico = Historico.new
      historico.solicitud_id = @solicitud.id
      historico.usuario_id = session[:usuario_id]
      historico.estado_id = 3
      historico.created_at = @solicitud.created_at
      historico.save      
    end
    historico = Historico.new
    historico.solicitud_id = @solicitud.id
    historico.usuario_id = session[:usuario_id]
    historico.estado_id = 5
    historico.save 
    if params[:motivo]
      @motivos = params[:motivo]      
      @motivos.each do |m|
        motivo = Motivo.find(m)
        historico.motivos << motivo
      end
    end
    ##
    flash[:alert] = "Solicitud nº #{@solicitud.id} con carta de pago #{@solicitud.pago.carta} #{@solicitud.pago.verificacion} rechazada"
    redirect_to solicitudes_verificar_path
  end
  
  def verificar_solicitud
    @solicitud = Solicitud.find(params[:id])
    @solicitud.estado_id = 4
    @solicitud.save
    ## Grabar en el histórico
    if Historico.where(solicitud_id: @solicitud.id).count == 0
      historico = Historico.new
      historico.solicitud_id = @solicitud.id
      historico.usuario_id = session[:usuario_id]
      historico.estado_id = 3
      historico.created_at = @solicitud.created_at
      historico.save      
    end
    historico = Historico.new
    historico.solicitud_id = @solicitud.id
    historico.usuario_id = session[:usuario_id]
    historico.estado_id = 4
    historico.save 
    ##
    @solicitud.motivos.delete_all
    flash[:notice] = "Solicitud nº #{@solicitud.id} con carta de pago #{@solicitud.pago.carta} #{@solicitud.pago.verificacion} validada"
    redirect_to solicitudes_verificar_path
  end
  
  def validar
    @carta_pago = params[:verificar][:carta_pago]
    @pago = Pago.where(carta: @carta_pago[0..11])
    if @pago.count == 0
      flash[:alert] = "No existe la carta de pago proporcionada"
      redirect_to solicitudes_verificar_path
    else
      @solicitud = @pago.first.solicitud
    end
    
  end
  
  
  def actualiza_municipios
    if params[:id].to_i != 0
      @codigo_postal = Provincia.find(params[:id]).id.to_s
      @codigo_postal = "0" + @codigo_postal if @codigo_postal.length == 1
    end
    respond_to do |format|
      format.js
    end
  end
  
  def actualiza_titulaciones
    respond_to do |format|
      format.js
    end
  end
  
  def actualiza_tasas
    respond_to do |format|
      format.js
    end
  end
  
  def actualiza_codigo_categoria
    @turno_id = params[:subid]
    @concurso_id = params[:id]
    params[:id] = Concurso.find(params[:id]).categoria.id
    respond_to do |format|
      format.js
    end
  end
  
  
  private
  def solicitud_params
    params.require(:solicitud).permit(:turno_id, :concurso_id, :nombre, :apellido1, :apellido2, 
      :categoria_id, :nif, :domicilio, :fecha_nacimiento,
      :telefono, :movil, :numero, :piso, :puerta, :codigo_postal,
      :nacionalidad_id, :sexo_id, :municipio_id, :titulacion_id, :permiso_id,
      :grado, :descripcion_discapacidad, :adaptacion)
  end
  
  def set_solicitud
    @solicitud = Solicitud.activo.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Solicitud"
    redirect_to solicitudes_path
  end
  
  def copiar_historico
    historico_solicitud = HistoricoSolicitud.new
    historico_solicitud.solicitud_id            = @solicitud.id
    historico_solicitud.nombre                  = @solicitud.nombre
    historico_solicitud.apellido1               = @solicitud.apellido1
    historico_solicitud.apellido2               = @solicitud.apellido2
    historico_solicitud.nif                     = @solicitud.nif
    historico_solicitud.fecha_nacimiento        = @solicitud.fecha_nacimiento
    historico_solicitud.telefono                = @solicitud.telefono
    historico_solicitud.movil                   = @solicitud.movil
    historico_solicitud.domicilio               = @solicitud.domicilio
    historico_solicitud.codigo_postal           = @solicitud.codigo_postal
    historico_solicitud.sexo_id                 = @solicitud.sexo_id
    historico_solicitud.nacionalidad_id         = @solicitud.nacionalidad_id
    historico_solicitud.municipio_id            = @solicitud.municipio_id
    historico_solicitud.turno_id                = @solicitud.turno_id
    historico_solicitud.concurso_id             = @solicitud.concurso_id
    historico_solicitud.titulacion_id           = @solicitud.titulacion_id
    historico_solicitud.permiso_id              = @solicitud.permiso_id
    historico_solicitud.grado                   = @solicitud.grado
    historico_solicitud.descripcion_discapacidad= @solicitud.descripcion_discapacidad
    historico_solicitud.adaptacion              = @solicitud.adaptacion
    historico_solicitud.pago_id                 = @solicitud.pago_id
    historico_solicitud.estado_id               = @solicitud.estado_id
    historico_solicitud.activo                  = @solicitud.activo
    historico_solicitud.save
    
    @solicitud.zonas.each do |z|
      historico_solicitud.zonas << z
    end
    
    historico = Historico.new
    historico.solicitud_id = @solicitud.id
    historico.usuario_id = session[:usuario_id]
    historico.estado_id = 10
    historico.save
  end
end
