class ConcursosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_concurso, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @concursos = Concurso.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: ConcursosDatatable.new(view_context) }
    end
  end
  
  def new
    @concurso = Concurso.new
    @titulaciones = Titulacion.all
  end
  
  def edit
  end
  
  def create
    @concurso = Concurso.new(concurso_params)
    if @concurso.save
      if params[:turnos]
        @turnos = params[:turnos]         
        @turnos.each do |t|
          turno = Turno.find(t)
          @concurso.turnos << turno
        end
      end
      if params[:titulaciones]
        @titulaciones = params[:titulaciones]         
        @titulaciones.each do |t|
          titulacion = Titulacion.find(t)
          @concurso.titulaciones << titulacion
        end
      end
      if params[:zonas]
        @zonas = params[:zonas]         
        @zonas.each do |t|
          zona = Zona.find(t)
          @concurso.zonas << zona
        end
      else
        (1..16).each do |t|
          zona = Zona.find(t)
          @concurso.zonas << zona
        end
      end
      
      if params[:turnos]
        @turnos = params[:turnos]         
        @turnos.each do |t|
          informix = Informix.new
          informix.concurso_id = @concurso.id
          informix.turno_id = t
          informix.codigo = params[:categoria_informix]
          Rails.logger.info "Categoría es #{@concurso.id}"
          Rails.logger.info "Turno es #{t}"
          Rails.logger.info "Parámetro categoria_informix es #{params[:categoria_informix]}"
          informix.save          
        end
      end
      
     
      flash[:notice] = "Concurso añadido correctamente"
      redirect_to concursos_path
    else
      flash[:alert] = "Hubo un error al añadir el Concurso"
      render "new"
    end
  end

  def update
    if @concurso.update(concurso_params)
      if params[:turnos]
        @concurso.turnos.destroy_all
        @turnos = params[:turnos]         
        @turnos.each do |t|
          turno = Turno.find(t)
          @concurso.turnos << turno
        end
      end
      if params[:titulaciones]
        @concurso.titulaciones.destroy_all
        @titulaciones = params[:titulaciones]         
        @titulaciones.each do |t|
          titulacion = Titulacion.find(t)
          @concurso.titulaciones << titulacion
        end
      end
      if params[:zonas]
        @concurso.zonas.destroy_all
        @zonas = params[:zonas]         
        @zonas.each do |t|
          zona = Zona.find(t)
          @concurso.zonas << zona
        end
      end
      flash[:notice] = "Concurso modificado correctamente"
      redirect_to concursos_path
    else
      flash[:alert] = "Hubo un error al modificar el Concurso"
      render "edit"
    end
  end
  
  def destroy
    @concurso.activo = false
    @concurso.save
    flash[:notice] = "Concurso borrada correctamente"
    redirect_to concursos_path
  end
  
  private
  def concurso_params
    params.require(:concurso).permit(:fecha_doe, :categoria_id, :tasa_id, :modelo_pdf, :titulaciones, :zonas)
  end
  
  def set_concurso
    @concurso = Concurso.activo.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Concurso"
    redirect_to concursos_path
  end
end
