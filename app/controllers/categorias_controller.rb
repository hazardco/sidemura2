class CategoriasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_categoria, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @categorias = Categoria.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: CategoriasDatatable.new(view_context) }
    end
  end
  
  def new
    @categoria = Categoria.new
  end
  
  def edit
  end
  
  def create
    @categoria = Categoria.new(categoria_params)
    if @categoria.save
      flash[:notice] = "Categoria añadida correctamente"
      redirect_to categorias_path
    else
      flash[:alert] = "Hubo un error al añadir el Categoria"
      render "new"
    end
  end

  def update
    if @categoria.update(categoria_params)
      flash[:notice] = "Categoria modificada correctamente"
      redirect_to categorias_path
    else
      flash[:alert] = "Hubo un error al modificar el Categoria"
      render "edit"
    end
  end
  
  def destroy
    @categoria.activo = false
    @categoria.save
    flash[:notice] = "Categoria borrada correctamente"
    redirect_to categorias_path
  end
  
  private
  def categoria_params
    params.require(:categoria).permit(:denominacion, :codigo, :permiso_id, :tasa_id, :activo)
  end
  
  def set_categoria
    @categoria = Categoria.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Categoria"
    redirect_to categorias_path
  end
end
