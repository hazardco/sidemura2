class MenusController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_menu, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @menus = Menu.activo.page params[:page]
  end
  
  def new
    @menu = Menu.new
  end
  
  def edit
  end
  
  def create
    @menu = Menu.new(menu_params)
    if @menu.save
      flash[:notice] = "Menu añadida correctamente"
      redirect_to menus_path
    else
      flash[:alert] = "Hubo un error al añadir el Menu"
      render "new"
    end
  end

  def update
    if @menu.update(menu_params)
      flash[:notice] = "Menu modificada correctamente"
      redirect_to menus_path
    else
      flash[:alert] = "Hubo un error al modificar el Menu"
      render "edit"
    end
  end
  
  def destroy
    @menu.activo = false
    @menu.save
    flash[:notice] = "Menu borrada correctamente"
    redirect_to menus_path
  end
  
  private
  def menu_params
    params.require(:menu).permit(:denominacion, :icono, :activo)
  end
  
  def set_menu
    @menu = Menu.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Menu"
    redirect_to menus_path
  end
end
