class MotivosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_motivo, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @motivos = Motivo.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: MotivosDatatable.new(view_context) }
    end
  end
  
  def new
    @motivo = Motivo.new
  end
  
  def edit
  end
  
  def create
    @motivo = Motivo.new(motivo_params)
    if @motivo.save
      flash[:notice] = "Motivo añadida correctamente"
      redirect_to motivos_path
    else
      flash[:alert] = "Hubo un error al añadir el Motivo"
      render "new"
    end
  end

  def update
    if @motivo.update(motivo_params)
      flash[:notice] = "Motivo modificada correctamente"
      redirect_to motivos_path
    else
      flash[:alert] = "Hubo un error al modificar el Motivo"
      render "edit"
    end
  end
  
  def destroy
    @motivo.activo = false
    @motivo.save
    flash[:notice] = "Motivo borrada correctamente"
    redirect_to motivos_path
  end
  
  private
  def motivo_params
    params.require(:motivo).permit(:denominacion, :activo)
  end
  
  def set_motivo
    @motivo = Motivo.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Motivo"
    redirect_to motivos_path
  end
end
