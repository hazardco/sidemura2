class ConvocatoriasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_convocatoria, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @convocatorias = Convocatoria.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: ConvocatoriasDatatable.new(view_context) }
    end
  end
  
  def new
    @convocatoria = Convocatoria.new
  end
  
  def edit
  end
  
  def create
    @convocatoria = Convocatoria.new(convocatoria_params)
    if @convocatoria.save
      flash[:notice] = "Convocatoria añadida correctamente"
      redirect_to convocatorias_path
    else
      flash[:alert] = "Hubo un error al añadir el Convocatoria"
      render "new"
    end
  end

  def update
    if @convocatoria.update(convocatoria_params)
      flash[:notice] = "Convocatoria modificada correctamente"
      redirect_to convocatorias_path
    else
      flash[:alert] = "Hubo un error al modificar el Convocatoria"
      render "edit"
    end
  end
  
  def destroy
    @convocatoria.activo = false
    @convocatoria.save
    flash[:notice] = "Convocatoria borrada correctamente"
    redirect_to convocatorias_path
  end
  
  private
  def convocatoria_params
    params.require(:convocatoria).permit(:denominacion, :fecha_doe, :activo)
  end
  
  def set_convocatoria
    @convocatoria = Convocatoria.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Convocatoria"
    redirect_to convocatorias_path
  end
end
