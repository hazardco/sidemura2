class SubmenusController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_submenu, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @submenus = Submenu.activo.page params[:page]
  end
  
  def new
    @submenu = Submenu.new
  end
  
  def edit
  end
  
  def create
    @submenu = Submenu.new(submenu_params)
    if @submenu.save
      flash[:notice] = "Submenu añadida correctamente"
      redirect_to submenus_path
    else
      flash[:alert] = "Hubo un error al añadir el Submenu"
      render "new"
    end
  end

  def update
    if @submenu.update(submenu_params)
      flash[:notice] = "Submenu modificada correctamente"
      redirect_to submenus_path
    else
      flash[:alert] = "Hubo un error al modificar el Submenu"
      render "edit"
    end
  end
  
  def destroy
    @submenu.activo = false
    @submenu.save
    flash[:notice] = "Submenu borrada correctamente"
    redirect_to submenus_path
  end
  
  private
  def submenu_params
    params.require(:submenu).permit(:denominacion, :icono, :enlace, :menu_id, :activo)
  end
  
  def set_submenu
    @submenu = Submenu.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Submenu"
    redirect_to submenus_path
  end
end
