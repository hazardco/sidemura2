class MunicipiosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_municipio, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @municipios = Municipio.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: MunicipiosDatatable.new(view_context) }
    end
  end
  
  def new
    @municipio = Municipio.new
  end
  
  def edit
  end
  
  def create
    @municipio = Municipio.new(municipio_params)
    if @municipio.save
      flash[:notice] = "Municipio añadida correctamente"
      redirect_to municipios_path
    else
      flash[:alert] = "Hubo un error al añadir el Municipio"
      render "new"
    end
  end

  def update
    if @municipio.update(municipio_params)
      flash[:notice] = "Municipio modificada correctamente"
      redirect_to municipios_path
    else
      flash[:alert] = "Hubo un error al modificar el Municipio"
      render "edit"
    end
  end
  
  def destroy
    @municipio.activo = false
    @municipio.save
    flash[:notice] = "Municipio borrada correctamente"
    redirect_to municipios_path
  end
  
  private
  def municipio_params
    params.require(:municipio).permit(:denominacion, :habitantes, :provincia_id, :activo)
  end
  
  def set_municipio
    @municipio = Municipio.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Municipio"
    redirect_to municipios_path
  end
end
