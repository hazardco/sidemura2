class InformixesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_informix, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @informixes = Informix.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: InformixesDatatable.new(view_context) }
    end
  end
  
  def new
    @informix = Informix.new
  end
  
  def edit
  end
  
  def create
    @informix = Informix.new(informix_params)
    if @informix.save
      flash[:notice] = "Informix añadida correctamente"
      redirect_to informixes_path
    else
      flash[:alert] = "Hubo un error al añadir el Informix"
      render "new"
    end
  end

  def update
    if @informix.update(informix_params)
      flash[:notice] = "Informix modificada correctamente"
      redirect_to informixes_path
    else
      flash[:alert] = "Hubo un error al modificar el Informix"
      render "edit"
    end
  end
  
  def destroy
    @informix.activo = false
    @informix.save
    flash[:notice] = "Informix borrada correctamente"
    redirect_to informixes_path
  end
  
  private
  def informix_params
    params.require(:informix).permit(:categoria_id, :turno_id, :codigo, :activo)
  end
  
  def set_informix
    @informix = Informix.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Informix"
    redirect_to informixes_path
  end
end
