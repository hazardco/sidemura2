class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def requiere_autenticacion!
    if current_user.nil?
      flash[:error] = "Necesitas entrar un Usuario y Contraseña correctos para continuar"
      redirect_to login_url
    end
  end
  helper_method :require_signin!

  def current_user
    @current_user ||= Usuario.find(session[:usuario_id]) if session[:usuario_id]
  end
  helper_method :current_user
  
  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = "Acceso denegado. No posee los suficientes permisos."
    redirect_to root_url
  end
end