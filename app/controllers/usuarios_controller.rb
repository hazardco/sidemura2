class UsuariosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_usuario, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @usuarios = Usuario.activo.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: UsuariosDatatable.new(view_context) }
    end
  end
  
  def new
    @usuario = Usuario.new
  end
  
  def edit
    
  end
  
  def create
    @usuario = Usuario.new(usuario_params)
    @usuario.roles = params[:usuario][:roles]
    if @usuario.save
      flash[:notice] = "Usuario añadido correctamente."
      redirect_to usuarios_path
    else
      flash[:alert] = "Hubo un error al añadir el Usuario"
      render "new"
    end
  end
  
  def update
    @usuario.roles = params[:usuario][:roles]
    if @usuario.update(usuario_params)
      flash[:notice] = "Usuario modificado correctamente"
      redirect_to usuarios_path
    else
      flash[:alert] = "Hubo un error al añadir el Usuario"
      render "edit"
    end
  end

  def show
    @usuario = Usuario.find(params[:id])
  end
  
  def destroy
    @usuario.activo = 0
    @usuario.save
    flash[:notice] = "Usuario borrado correctamente"
    redirect_to usuarios_path
  end
  
  private
  def usuario_params
    params.require(:usuario).permit(:nombre, :apellidos, :correo, :usuario, :password, :password_confirmation, :roles)
  end
  def set_usuario
    @usuario = Usuario.activo.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Usuario"
    redirect_to usuarios_path
  end
end