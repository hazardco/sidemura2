class Historico < ActiveRecord::Base      
  belongs_to :solicitud
  belongs_to :usuario
  belongs_to :estado
  has_and_belongs_to_many :motivos
  
  def motivos_rechazo
    motivos_array = Array.new
      motivos.each do |m|
        motivos_array << m.denominacion
      end
    motivos_array.join(", ")
  end
end