class Pago < ActiveRecord::Base
    has_one :solicitud
  def obtener_numero
    @OFFSET_CARTA_PAGO = "05307000001"
    id + @OFFSET_CARTA_PAGO.to_i
  end
  
  def obtener_digito_control
    modulo = obtener_numero % 7
    if modulo == 0
      0
    else
      7 - modulo
    end 
  end
end
