class Convocatoria < ActiveRecord::Base      
  scope :activo, -> { where(activo: true) }
      
    validates :fecha_doe, presence: true
          
    validates :denominacion, presence: true
    
    has_many :concursos

end
