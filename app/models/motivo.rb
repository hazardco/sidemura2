class Motivo < ActiveRecord::Base      
  scope :activo, -> { where(activo: true) }
      
    validates :denominacion, presence: true
    
end