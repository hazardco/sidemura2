class Municipio < ActiveRecord::Base      
  scope :activo, -> { where(activo: true) }
      
    validates :provincia, presence: true
    
      
    validates :habitantes, presence: true
    
      
    validates :denominacion, presence: true
    

  belongs_to :provincia
end
