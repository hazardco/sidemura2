class Categoria < ActiveRecord::Base
  scope :activo, -> { where(activo: true) }

  belongs_to :tasa
  belongs_to :permiso
  
end