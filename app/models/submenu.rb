class Submenu < ActiveRecord::Base      
  scope :activo, -> { where(activo: true) }

  belongs_to :menu
end
