class Informix < ActiveRecord::Base      
  scope :activo, -> { where(activo: true) }
      
    validates :codigo, presence: true    
          

  belongs_to :concurso
  belongs_to :turno
end
