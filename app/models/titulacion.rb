class Titulacion < ActiveRecord::Base
  scope :activo, -> { where(activo: true) }

  has_many :concursos
  
end
