class HistoricoSolicitud < ActiveRecord::Base      
  scope :activo, -> { where(activo: true) }
      
    validates :estado, presence: true
    
      
    validates :pago, presence: true
    
      
    validates :nif, presence: true
    
      
    validates :turno, presence: true
    
      
    validates :concurso, presence: true
    

  belongs_to :concurso
  belongs_to :turno
  belongs_to :pago
  belongs_to :estado
  belongs_to :municipio
  belongs_to :nacionalidad
  belongs_to :sexo
  belongs_to :titulacion
  belongs_to :permiso
  has_and_belongs_to_many :zonas
  has_and_belongs_to_many :motivos

end