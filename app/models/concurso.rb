class Concurso < ActiveRecord::Base
  scope :activo, -> { where(activo: true) }

  validates :modelo_pdf, presence: true
            
  belongs_to :categoria
  belongs_to :convocatoria

  belongs_to :pago
  has_and_belongs_to_many :titulaciones
  has_and_belongs_to_many :zonas
  has_and_belongs_to_many :turnos

  belongs_to :tasa
  
  def con
    categoria.denominacion
  end
end