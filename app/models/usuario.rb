class Usuario < ActiveRecord::Base
  has_secure_password

  paginates_per 10
  validates :nombre, presence: true
  validates :apellidos, presence: true
  validates :correo, presence: true
  validates :usuario, presence: true
  validates :correo, uniqueness: true
  validates :usuario, uniqueness: true

  scope :activo, -> { where(activo: true) }
  
  scope :con_rol, lambda { |role| {:conditions => "roles_mask & #{2**ROLES.index(role.to_s)} > 0 "} }  

  ROLES = %w[usuario administrador gestor]

  def rol_symbols  
    roles.map(&:to_sym)  
  end  
  
  def roles=(roles)  
    self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.sum  
  end  
  
  def roles  
    ROLES.reject { |r| ((roles_mask || 0) & 2**ROLES.index(r)).zero? }  
  end
  
  def rol?(rol)  
    roles.include? rol.to_s  
  end  
end