require 'open-uri'


prawn_document(:top_margin => 75, :page_size => "LEGAL", :page_layout => :landscape) do |pdf|

  pdf.font_size 16
  if params[:estado] == 5.to_s
    pdf.text "Listado de solicitudes rechazadas"
  else
    pdf.text "Listado de solicitudes aceptadas"
  end
  pdf.move_down 10
  pdf.text @grupo + " - " + @concurso + ". Turno: " + @turno 
  pdf.move_down 20
  pdf.font_size 10
  
  orden = 1
  if params[:estado] == 5.to_s
    tabla  = [["#","APELLIDOS", "NOMBRE", "NIF/NIE", "MOTIVO"]]
    @solicitudes.each do |s|
      motivos = Array.new
      s.motivos.each do |m|
        motivos << m.denominacion
      end
      tabla += [[orden, s.apellido1.mb_chars.upcase.to_s + " " + s.apellido2.mb_chars.upcase.to_s, s.nombre.mb_chars.upcase.to_s, s.nif.mb_chars.upcase.to_s, motivos.join(",")]]
      orden = orden +1
    end
    pdf.table(tabla, :cell_style => {:border_width => 0, :width => 200}, :row_colors => ["F0F0F0", "FFFFCC"], :header => true)
  else
    if @turno == "Discapacidad"
      tabla  = [["#","APELLIDOS", "NOMBRE", "NIF/NIE", "ADAPTACIÓN"]]
      @solicitudes.each do |s|
        tabla += [[orden, s.apellido1.mb_chars.upcase.to_s + " " + s.apellido2.mb_chars.upcase.to_s, s.nombre.mb_chars.upcase.to_s, s.nif.mb_chars.upcase.to_s, s.adaptacion]]
        orden = orden +1
      end
      pdf.table(tabla, :cell_style => {:border_width => 0, :width => 150}, :row_colors => ["F0F0F0", "FFFFCC"], :header => true)
    else
      tabla  = [["#","APELLIDOS", "NOMBRE", "NIF/NIE"]]
      @solicitudes.each do |s|
        tabla += [[orden, s.apellido1.mb_chars.upcase.to_s + " " + s.apellido2.mb_chars.upcase.to_s, s.nombre.mb_chars.upcase.to_s, s.nif.mb_chars.upcase.to_s]]
        orden = orden +1
      end
      pdf.table(tabla, :cell_style => {:border_width => 0, :width => 200}, :row_colors => ["F0F0F0", "FFFFCC"], :header => true)
    end
  end
end