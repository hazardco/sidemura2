$(document).ready(function() {
    $("#titulacion_id").select2();
    $("#titulacion_id").change(function() {
        indice_titulacion = $("#titulacion_id").val();
        titulacion = $('#titulacion_id option[value="' + indice_titulacion + '"]').text();
        titulacion_info = '<a href="#" data-dismiss="alert" class="close">×</a><i class="icon-circle-blank"></i> ' + titulacion + '<input name="titulaciones[]" value="' + indice_titulacion + '" type="hidden">';
        titulacion_info_completa = '<div class="alert alert-info alert-dismissable"><a href="#" data-dismiss="alert" class="close">×</a><i class="icon-circle-blank"></i> ' + titulacion + '<input type="hidden" name="titulaciones[]" value="' + indice_titulacion + '"></div>';
        anadir = 1;
        $('#tabla_titulaciones div').each(function(index) {
            if ($(this).html() == titulacion_info)
                anadir = 0;
        });
        if (anadir == 1) {
            $("#tabla_titulaciones").append(titulacion_info_completa);
        }
        return false;
    });
});