$(document).ready(function() {
    $("#zona_id").select2();
    $("#zona_id").change(function() {
        indice_zona = $("#zona_id").val();
        zona = $('#zona_id option[value="' + indice_zona + '"]').text();
        zona_info = '<a href="#" data-dismiss="alert" class="close">×</a><i class="icon-circle-blank"></i> ' + zona + '<input name="zonas[]" value="' + indice_zona + '" type="hidden">';
        zona_info_completa = '<div class="alert alert-warning alert-dismissable"><a href="#" data-dismiss="alert" class="close">×</a><i class="icon-circle-blank"></i> ' + zona + '<input type="hidden" name="zonas[]" value="' + indice_zona + '"></div>';
        anadir = 1;
        $('#tabla_zonas div').each(function(index) {
            if ($(this).html() == zona_info)
                anadir = 0;
        });
        if (anadir == 1) {
            $("#tabla_zonas").append(zona_info_completa);
        }
        return false;
    });
});