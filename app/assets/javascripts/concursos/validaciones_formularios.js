$(document).ready(function() {
    $("#new_concurso").validate({
        rules: {            
            "concurso[modelo_pdf]": {
                required: true,
                minlength: 1,          
            },
        }
    });
    $("#edit_concurso").validate({
        rules: {            
            "concurso[modelo_pdf]": {
                required: true,
                minlength: 1,         
            },
        }
    });
});