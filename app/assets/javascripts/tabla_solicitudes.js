$(document).ready(function() {
    function actualizar_solicitudes() {
        var grupo = $('select#grupo_id :selected').val();
        var concurso = $('select#concurso_id :selected').val();
        var turno = $('select#turno_id :selected').val();
        var estado = $('select#estado_id :selected').val();
        var nif_carta_pago = $('#nif_carta_pago').val();

        if (!grupo) {
            grupo = 0;
        }
        if (!concurso) {
            concurso = 0;
        }
        if (!turno) {
            turno = 0;
        }
        if (!estado) {
            estado = 0;
        }
        if (!nif_carta_pago) {
            nif_carta_pago = 0;
        }
        jQuery.get('/solicitudes?concurso=' + concurso + '&grupo=' + grupo + '&turno=' + turno + '&estado=' + estado + '&nif_carta_pago=' + nif_carta_pago, function(data) {
        });
    }

    function actualizar_concurso() {
        var grupo = $('select#grupo_id :selected').val();
        jQuery.get('/solicitudes/actualizar_concurso?grupo=' + grupo, function(data) {
        });

    }

    $("#grupo_id").change(function() {
        actualizar_solicitudes();
        actualizar_concurso();
        return false;
    });
    $("#concurso_id").change(function() {
        actualizar_solicitudes();
        return false;
    });
    $("#turno_id").change(function() {
        actualizar_solicitudes();
        return false;
    });
    $("#estado_id").change(function() {
        actualizar_solicitudes();
        return false;
    });
    $("#nif_carta_pago").keyup(function() {
        actualizar_solicitudes();
        return false;
    });
});
