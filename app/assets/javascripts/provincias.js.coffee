jQuery -> 
      dt = $('#provincias').dataTable
        sDom: "<'row datatables-top'<'col-sm-6'l><'col-sm-6 text-right'f>r>t<'row datatables-bottom'<'col-sm-6'i><'col-sm-6 text-right'p>>"
        sPaginationType: "bootstrap"
        bFilter: true
        bProcessing: true
        bServerSide: true
        sAjaxSource: $('#provincias').data('source')
        oLanguage:
            sLengthMenu: "_MENU_ registros por página"
        "aoColumns": [
            null,
            
            null,
            
            { "bSortable": false },
        ]
        "iDisplayLength": $('#provincias').data("pagination-records") || 10
        oLanguage:
          sLengthMenu: "_MENU_ registros por página"

      dt.columnFilter() if $('#provincias').hasClass("data-table-column-filter")
      dt.closest('.dataTables_wrapper').find('div[id$=_filter] input').css("width", "200px");
      dt.closest('.dataTables_wrapper').find('input').addClass("form-control input-sm").attr('placeholder', 'Buscar')