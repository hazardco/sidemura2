$(document).ready(function() {
    getRandomData = function(opcion, numero) {
        var i, prev, res, y;
        datareal = [];
        switch(opcion) {
            case(1):
                datareal = solicitudes_nuevas;
                break;
                case(2):
                datareal = solicitudes_verificadas;
                break;
                case(3):
                datareal = solicitudes_pdfs;
                break;
        }
        if (datareal.length > 0) {
            datareal = datareal.slice(1);
        }
        while (datareal.length < totalPoints) {
            prev = (datareal.length > 0 ? datareal[datareal.length - 1] : 50);
            y = numero;
            datareal.push(y);
        }
        res = [];
        i = 0;
        while (i < datareal.length) {
            res.push([i, datareal[i]]);
            ++i;
        }
        switch(opcion) {
            case(1):
                solicitudes_nuevas = datareal.slice();
                break;
                case(2):
                solicitudes_verificadas = datareal.slice();
                break;
                case(3):
                solicitudes_pdfs = datareal.slice();
                break;
        }
        return res;
    };
    solicitudes_nuevas = [];
    solicitudes_verificadas = [];
    solicitudes_pdfs = [];

    totalPoints = 300;
    updateInterval = 2000;
    var dataset = [
            { label: "Solicitudes Nuevas", data: getRandomData(1, 0) },
            { label: "Solicitudes Verificadas", data: getRandomData(2, 0) },
            { label: "Solicitudes en PDF", data: getRandomData(3, 0) }
        ];
    options = {
        series: {
            shadowSize: 0
        },
        yaxis: {
            min: 0,
            max: 250
        },
        xaxis: {
            show: false
        }
    };
    
    jQuery.get('/dashboard/charts', function(data) {
        });
        
    
    
    update = function() {
        jQuery.get('/dashboard/real', function(data) {
        });
        jQuery.get('/dashboard/charts', function(data) {
        });
        /*return setTimeout(update, updateInterval);*/
        return update;
    };

    plot = $.plot($("#stats-chart3"), dataset, options);
    update();

});


