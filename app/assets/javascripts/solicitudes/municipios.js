jQuery(function($) {
    $("#provincia_id").change(function() {
        var provincia = $('select#provincia_id :selected').val();
        if (provincia) {
            jQuery.get('/solicitudes/actualiza_municipios/' + provincia, function(data) {
            });
        } else {
            jQuery.get('/solicitudes/actualiza_municipios/0', function(data) {
            });
        }
        return false;
    });
});