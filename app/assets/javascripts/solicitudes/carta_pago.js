(function($) {

    jQuery.validator.addMethod("carta_pago", function(carta_pago, element) {
        //alert(carta_pago);
        var numero = carta_pago.substr(0, 12);
        var codigo = carta_pago.substr(12, 1);
        var codigo_a_verificar;
        var modulo = numero % 7;
        if (modulo == 0) {
            codigo_a_verificar = 0;
        } else {
            codigo_a_verificar = 7 - modulo;
        }
        if (codigo_a_verificar == codigo) {
            return true;
        } else {
            return false;
        }
    }, "Introduce una carta de pago válida");
}(jQuery));