$(document).ready(function() {
    $('#ficheros_carta_pago').focus();
    $("#formulario_ficheros").validate({
        rules: {
            "ficheros[carta_pago]": {
                required: true,
                digits: true,
                minlength: 13,
                maxlength: 13,
                carta_pago: true,
            },             
        }
    });
});