jQuery(function($) {
    $("#solicitud_nif").focusout(function() {
        var nif = $('#solicitud_nif').val().toUpperCase();
        if (/^[0-9]{8}[A-Z]{1}$/.test(nif)) {
            if ("TRWAGMYFPDXBNJZSQVHLCKE".charAt(nif.substring(8, 0) % 23) === nif.charAt(8)) {
                $('#solicitud_nacionalidad_id option:contains("España")').prop('selected', true);
                $('#solicitud_nacionalidad_id').prop('disabled', 'disabled');
            }
        }
        if (/^[XYZ]{1}/.test(nif)) {
            if (nif[ 8 ] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(nif.replace('X', '0').replace('Y', '1').replace('Z', '2').substring(0, 8) % 23)) {
                $('#solicitud_nacionalidad_id option:contains("España")').prop('disabled', true);
                $('#solicitud_nacionalidad_id option:contains("Selecciona la nacionalidad")').prop('selected', true);
                $('#solicitud_nacionalidad_id').prop('disabled', false);
            }
        }
    });
});