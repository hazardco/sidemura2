(function($) {
    jQuery.validator.addMethod("movil", function(movil, element) {
        if (movil.length > 0) {
            return movil.match(/[6|7|9|8][0-9]{8}$/);
        } else {
            return true;
        }
    }, "Introduce un teléfono correcto. Debe empezar por 6, 7, 8 ó 9.");
}(jQuery));