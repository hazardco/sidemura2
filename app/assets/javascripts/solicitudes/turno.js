jQuery(function($) {
    $("#solicitud_turno_id").change(function() {
        var turno = $('select#solicitud_turno_id :selected').val();
        var categoria = $('select#solicitud_concurso_id :selected').val();

        if (turno == 2) {
            $('#discapacidad').fadeIn('slow').removeClass('hide');
        } else {
            $('#discapacidad').fadeOut('slow');
        }
        jQuery.get('/solicitudes/actualiza_codigo_categoria/' + categoria + '/' + turno, function(data) {           
        });

    });
});