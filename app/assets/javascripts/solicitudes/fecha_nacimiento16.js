(function($) {
    jQuery.validator.addMethod("fecha_nacimiento16", function(fecha_nacimiento, element) {
          
        var fecha_nacimiento = moment(fecha_nacimiento, "DD/MM/YYYY","es");
        var fecha_inicio = moment("20/01/1949", "DD/MM/YYYY","es");
        var fecha_fin = moment("21/01/1996", "DD/MM/YYYY","es");

        if (fecha_nacimiento.isAfter(fecha_inicio) && fecha_nacimiento.isBefore(fecha_fin)) {
            return fecha_nacimiento;
        } else {
            return false;
        }
    }, "Introduce una fecha entre el 20/01/1949 y el 21/01/1996");
}(jQuery));

