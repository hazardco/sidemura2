(function($) {
    jQuery.validator.addMethod("descripcion_discapacidad", function(value, element) {
        "use strict";
        var grado = $('#solicitud_grado').val();
        if (grado && grado > 32 && grado < 100) {
            return value;
        }
        if (!grado) {
            return true;
        }
        return false;

    }, "Introduce una descripción de la discapacidad.");

}(jQuery));