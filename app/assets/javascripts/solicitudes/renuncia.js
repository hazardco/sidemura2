$(document).ready(function() {
    $("#RENUNCIA").change(function() {
        if ($(this).is(':checked')) {
            $('input[name="zona[]"]').attr('checked', false);
            $('input[name="zona[]"]').attr('disabled', true);
        } else {
            $('input[name="zona[]"]').attr('disabled', false);
        }
    });
});