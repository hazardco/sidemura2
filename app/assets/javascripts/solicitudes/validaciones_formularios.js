$(document).ready(function() {
    $('#verificar_carta_pago').focus();
    $("#formulario_verificar").validate({
        rules: {
            "verificar[carta_pago]": {
                required: true,
                digits: true,
                minlength: 13,
                maxlength: 13,
                carta_pago: true,
            },             
        }
    });
});