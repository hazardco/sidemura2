$(document).ready(function() {
    $("#new_solicitud").validate({
        rules: {
            "solicitud[apellido1]": {
                required: true,
                minlength: 2,
            },
            "solicitud[nombre]": {
                required: true,
                minlength: 2,
            },
            "solicitud[nif]": {
                required: true,
                nifES: true,
            },
            "fecha_nacimiento": {
                required: true,
                fecha: true,
                fecha_nacimiento: true,
            },
            "fecha_nacimiento16": {
                required: true,
                fecha: true,
                fecha_nacimiento16: true,
            },
            "solicitud[grado]": {
                number: true,
                range: [33, 99],
                required: true,             
            },
            "solicitud[telefono]": {
                number: true,
                minlength: 9,
                maxlength: 9,
                movil: true,
            },
            "solicitud[movil]": {
                number: true,
                minlength: 9,
                maxlength: 9,
                movil: true,
            },
            "solicitud[domicilio]": {
                required: true,
            },
            "provincia[id]": {
                required: true,
            },
            "LOPD": {
                required: true,
            },
            "solicitud[municipio_id]": {
                required: true,
            },
            "solicitud[sexo_id]": {
                required: true,
            },
            "solicitud[nacionalidad_id]": {
                required: true,
            },
            "solicitud[permiso_id]": {
                required: true,
            },
            "solicitud[codigo_postal]": {
                required: true,
            },
            "solicitud[descripcion_discapacidad]": {
                descripcion_discapacidad: true,
                minlength: 5,
            }
        }
    });
});