$(document).ready(function() {
    $("#new_convocatoria").validate({
        rules: {
            "convocatoria[fecha_doe]": {
                fecha: true,
                required: true,
                minlength: 10,
            },  
            "convocatoria[denominacion]": {
                required: true,
                minlength: 10,
            },
        }
    });
    $("#edit_convocatoria").validate({
        rules: {
            "convocatoria[fecha_doe]": {
                fecha: true,
                required: true,
                minlength: 10,
            },
            "convocatoria[denominacion]": {
                required: true,
                minlength: 10,
            },
        }
    });
});