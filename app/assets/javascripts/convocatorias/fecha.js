(function($) {

    jQuery.validator.addMethod("fecha", function(fecha, element) {
        fecha = fecha.replace(/-/g, "/");

        var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
        var matchArray = fecha.match(datePat); // is the format ok?

        month = matchArray[3]; // p@rse date into variables
        day = matchArray[1];
        year = matchArray[5];
        
        if (month < 1 || month > 12) { // check month range
            return false;
        }

        if (day < 1 || day > 31) {
            return false;
        }

        if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
            return false;
        }

        if (month == 2) { // check for february 29th
            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
            if (day > 29 || (day == 29 && !isleap)) {
                return false;
            }
        }
        return true; 

    }, "Introduce una fecha válida");
}(jQuery));